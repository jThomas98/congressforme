# Congress For Me

# name, EID, and GitLab ID, of all members
Pranav Neravetla

EID: prn289

Gitlab ID: pranavneravetla



Jason Thomas

EID: jit388

Gitlab ID: jThomas98



Ryan Menghani

EID: rvm447

Gitlab ID: menghaniryan



Darrius Anderson

EID: dea842

Gitlab ID: darrius35



Jorge Garcia

EID: jeg4252

Gitlab ID: jgarcia525



# Git SHA
eef7d49bff4c1ea15e34561493921efaa1b38127

# Project Leader
Jason Thomas

# link to GitLab pipelines
https://gitlab.com/jThomas98/congressforme/pipelines

# link to website
https://www.congressfor.me

# Estimated and actual time to complete for all members
Pranav Neravetla

Estimated time to complete: 20 hours

Actual time to complete: 18 hours



Jason Thomas

Estimated time to complete: 23 hours

Actual time to complete: 24 hours



Ryan Menghani

Estimated time to complete: 19 hrs

Actual time to complete: 22 hrs



Darrius Anderson

Estimated time to complete: 24 hrs

Actual time to complete: 22 hrs



Jorge Garcia

Estimated time to complete: 19 hours

Actual time to complete: 17 hours

