from flask import Flask, jsonify, request
import flask_restless

app = Flask(__name__)

from sqlalchemy import create_engine, String, Integer, Column, ForeignKey
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session, relationship

# Connect to database
engine_string = "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
engine = create_engine(engine_string)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
mysession = scoped_session(Session)
base = declarative_base()
base.metadata.bind = engine


class Congressman(base):
    __tablename__ = "congressmen"

    lastname = Column(String)
    firstname = Column(String)
    party = Column(String)
    state = Column(String)
    district = Column(Integer)
    districtid = Column(Integer)
    twitter = Column(String)
    lastsession = Column(Integer)
    yearsincumbent = Column(Integer)
    congressid = Column(Integer, primary_key=True)
    officialid = Column(String)
    crp_id = Column(String)
    pic = Column(String)
    dateofbirth = Column(String)
    facebook = Column(String)
    youtube = Column(String)
    url = Column(String)
    phone = Column(String)
    fullname = Column(String)
    congressdonor = relationship("CongressDonor", cascade="all, delete-orphan")
    donors = association_proxy(
        "congressdonor", "donor", creator=lambda tag: CongressDonor(donor=donor)
    )

    congresscommittee = relationship("CongressCommittee", cascade="all, delete-orphan")

    committees = association_proxy(
        "congresscommittee",
        "committee",
        creator=lambda tag: CongressCommittee(committee=committee),
    )

class StateReps(base):
    __tablename__ = "state_reps"
    state = Column(String, primary_key=True)
    count = Column(Integer)

class StateMoney(base):
    __tablename__ = "state_money"
    state = Column(String, primary_key=True)
    totalmoney = Column(Integer)

class PartyMoney(base):
    __tablename__ = "party_money"
    party = Column(String, primary_key=True)
    amount = Column(Integer)

class District(base):
    __tablename__ = "districts"

    districtid = Column(Integer, primary_key=True)
    state = Column(String)
    name = Column(String)
    number = Column(String)
    congressid = Column(Integer)
    party = Column(String)
    election_date = Column(String)
    wiki = Column(String)
    pic = Column(String)


class Donors(base):
    __tablename__ = "donors"

    donorid = Column(Integer, primary_key=True)
    cycle = Column(Integer)
    name = Column(String)
    dem_contributions = Column(Integer)
    rep_contributions = Column(Integer)
    orgid = Column(String)
    pic = Column(String)
    congressdonor = relationship("CongressDonor", cascade="all, delete-orphan")
    congressmen = association_proxy(
         "congressdonor", "congressman", creator=lambda tag: CongressDonor(congressman=congressman)
     )



class CongressDonor(base):
    __tablename__ = "congress_donor"

    congressid = Column(Integer, ForeignKey("congressmen.congressid"), primary_key=True)

    donorid = Column(Integer, ForeignKey("donors.donorid"), primary_key=True)

    amount = Column(Integer)

    donor = relationship("Donors")

    congressman = relationship("Congressman")


class Committee(base):
    __tablename__ = "committees"

    committeeid = Column(Integer, primary_key=True)
    name = Column(String)


class CongressCommittee(base):
    __tablename__ = "congress_committee"

    congressid = Column(Integer, ForeignKey("congressmen.congressid"), primary_key=True)

    committeeid = Column(
        Integer, ForeignKey("committees.committeeid"), primary_key=True
    )

    committee = relationship("Committee")


def add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    # Set whatever other headers you like...
    return response


@app.teardown_appcontext
def shutdown_session(exception=None):
    mysession.remove()


# Create database session

base.metadata.create_all(engine)

manager = flask_restless.APIManager(app, session=mysession)

congressmen_blueprint = manager.create_api_blueprint(Congressman, methods=["GET"])
congressmen_blueprint.after_request(add_cors_headers)

state_reps_blueprint = manager.create_api_blueprint(StateReps, methods=["GET"])
state_reps_blueprint.after_request(add_cors_headers)

state_money_blueprint = manager.create_api_blueprint(StateMoney, methods=["GET"])
state_money_blueprint.after_request(add_cors_headers)

party_money_blueprint = manager.create_api_blueprint(PartyMoney, methods=["GET"])
party_money_blueprint.after_request(add_cors_headers)

district_blueprint = manager.create_api_blueprint(District, methods=["GET"])

district_blueprint.after_request(add_cors_headers)

donor_blueprint = manager.create_api_blueprint(Donors, methods=["GET"])

donor_blueprint.after_request(add_cors_headers)

committees_blueprint = manager.create_api_blueprint(Committee, methods=["GET"])

committees_blueprint.after_request(add_cors_headers)

app.register_blueprint(congressmen_blueprint)
app.register_blueprint(district_blueprint)
app.register_blueprint(donor_blueprint)
app.register_blueprint(committees_blueprint)
app.register_blueprint(state_reps_blueprint)
app.register_blueprint(state_money_blueprint)
app.register_blueprint(party_money_blueprint)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
