import requests
import json
from urllib.parse import quote
import sqlalchemy as db
import time

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
congressmen = db.Table("congressmen", metadata, autoload=True, autoload_with=engine)
donors = db.Table("donors", metadata, autoload=True, autoload_with=engine)
congress_donor = db.Table(
    "congress_donor", metadata, autoload=True, autoload_with=engine
)


q = db.select([congressmen.columns.crp_id])
ResultProxy = connection.execute(q)
ResultSet = ResultProxy.fetchall()

for entry in ResultSet:
    cid = entry[0]
    if cid:
        response = requests.get(
            "https://www.opensecrets.org/api/?method=candContrib&cid="
            + cid
            + "&output=json&apikey=c1f22d5464a0c15a258fbdca31a2055d"
        )
        if response.status_code == 200:
            donorlist = json.loads(response.content)
            query = db.select([congressmen.columns.congressid]).where(
                congressmen.columns.crp_id == cid
            )
            CongressIdProxy = connection.execute(query)
            CongressResultSet = CongressIdProxy.fetchall()
            congressid = CongressResultSet[0][0]
            print(congressid)

            for org in donorlist["response"]["contributors"]["contributor"]:
                name = org["@attributes"]["org_name"]
                contrib = org["@attributes"]["total"]
                print(name)
                q = db.select([donors.columns.orgid])
                q = q.where(donors.columns.name == name)
                ResultProxy = connection.execute(q)
                ResultSet = ResultProxy.fetchall()
                if len(ResultSet) == 0:
                    idresponse = requests.get(
                        "https://www.opensecrets.org/api/?method=getOrgs&org="
                        + quote(name)
                        + "&output=json&apikey=c1f22d5464a0c15a258fbdca31a2055d"
                    )
                else:
                    orgid = ResultSet[0][0]
                if len(ResultSet) > 0 or idresponse.status_code == 200:
                    if len(ResultSet) == 0:
                        idjson = idresponse.json()
                        if len(idjson["response"]["organization"]) == 1:
                            orgid = idjson["response"]["organization"]["@attributes"][
                                "orgid"
                            ]
                        else:
                            i = 0
                            while (
                                i < len(idjson["response"]["organization"])
                                and idjson["response"]["organization"][i][
                                    "@attributes"
                                ]["orgname"]
                                != name
                            ):
                                i += 1
                            if i == len(idjson["response"]["organization"]):
                                orgid = idjson["response"]["organization"][i - 1][
                                    "@attributes"
                                ]["orgid"]
                            else:
                                orgid = idjson["response"]["organization"][i][
                                    "@attributes"
                                ]["orgid"]

                        orgresponse = requests.get(
                            "https://www.opensecrets.org/api/?method=orgSummary&id="
                            + orgid
                            + "&output=json&apikey=c1f22d5464a0c15a258fbdca31a2055d"
                        )
                        if orgresponse.status_code == 200:
                            orgjson = orgresponse.json()
                            cycle = orgjson["response"]["organization"]["@attributes"][
                                "cycle"
                            ]
                            dems = orgjson["response"]["organization"]["@attributes"][
                                "dems"
                            ]
                            repubs = orgjson["response"]["organization"]["@attributes"][
                                "repubs"
                            ]
                            print(
                                name
                                + " "
                                + cycle
                                + " "
                                + dems
                                + " "
                                + repubs
                                + " "
                                + orgid
                            )
                            query = db.insert(donors).values(
                                cycle=cycle,
                                name=name,
                                dem_contributions=dems,
                                rep_contributions=repubs,
                                orgid=orgid,
                            )
                            ResultProxy = connection.execute(query)
                    if len(ResultSet) > 0 or orgresponse.status_code == 200:
                        query = db.select([donors.columns.donorid]).where(
                            donors.columns.orgid == orgid
                        )
                        DonorIdProxy = connection.execute(query)
                        DonorResultSet = DonorIdProxy.fetchall()
                        donorid = DonorResultSet[0][0]
                        q = db.select([congress_donor])
                        q = q.where(
                            db.and_(
                                congress_donor.columns.congressid == congressid,
                                congress_donor.columns.donorid == donorid,
                            )
                        )
                        ResultProxy = connection.execute(q)
                        ResultSet = ResultProxy.fetchall()
                        if len(ResultSet) == 0:
                            print(
                                str(congressid)
                                + " "
                                + str(donorid)
                                + " "
                                + str(contrib)
                            )
                            query = db.insert(congress_donor).values(
                                congressid=congressid, donorid=donorid, amount=contrib
                            )
                            ResultProxy = connection.execute(query)
