# Script to make an API request to get all the committees in house
# and adding it to the committees table in the database.

import requests
import json
import sqlalchemy as db

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
committees = db.Table("committees", metadata, autoload=True, autoload_with=engine)

url = "https://api.propublica.org/congress/v1/116/house/committees.json"

payload = ""
headers = {
    "x-api-key": "ehoMM6ZlyPycaUnQ15p1KEVrQzE6fkTiAWhJznlc",
    "User-Agent": "PostmanRuntime/7.18.0",
    "Accept": "*/*",
    "Cache-Control": "no-cache",
    "Postman-Token": "f0af1190-ac64-4f2b-9a52-a906da17af9d,6245885e-a05c-4815-91a9-4b485445da0c",
    "Host": "api.propublica.org",
    "Accept-Encoding": "gzip, deflate",
    "Connection": "keep-alive",
    "cache-control": "no-cache",
}

response = requests.request("GET", url, data=payload, headers=headers)

response = response.json()

for i in response["results"][0]["committees"]:
    query = db.insert(committees).values(name=i["name"])
    ResultProxy = connection.execute(query)
