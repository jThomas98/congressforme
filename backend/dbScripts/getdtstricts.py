import json
import datetime
import sqlalchemy as db

engine = db.create_engine(
    "postgresql+psycopg2://congress:PASSWORD_GOES_HERE@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
districts = db.Table("districts", metadata, autoload=True, autoload_with=engine)

# json file for the district info downloaded from https://github.com/unitedstates/congress-legislators
with open("legislators-current.json") as json_file:
    data = json.load(json_file)
    for p in data:
        for term in p["terms"]:
            if term["type"] == "rep":
                dateList = term["end"].split("-")
                termDate = datetime.date(
                    int(dateList[0]), int(dateList[1]), int(dateList[2])
                )
                if termDate > datetime.date.today():
                    print(
                        term["state"]
                        + " "
                        + str(term["district"])
                        + " "
                        + term["party"]
                        + " "
                        + str(termDate)
                    )
                    query = db.insert(districts).values(
                        state=term["state"],
                        number=term["district"],
                        party=term["party"],
                        election_date=str(termDate),
                    )
                    ResultProxy = connection.execute(query)
        print("name: " + p["name"]["official_full"])
