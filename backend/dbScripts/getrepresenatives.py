# Script to make an API request to get all the representatives and their information
# and adding it to the congressmen table in the database.

import requests
import json
import sqlalchemy as db

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
congressmen = db.Table("congressmen", metadata, autoload=True, autoload_with=engine)

url = "https://api.propublica.org/congress/v1/116/house/members.json"

partyDict = {"R": "Republican", "D": "Democrat", "I": "Independent"}

headers = {
    "x-api-key": "ehoMM6ZlyPycaUnQ15p1KEVrQzE6fkTiAWhJznlc",
    "User-Agent": "PostmanRuntime/7.18.0",
    "Accept": "*/*",
    "Cache-Control": "no-cache",
    "Postman-Token": "725e3d24-ddff-4a5f-ac36-01116083ac26,adaed41e-f9e2-4333-ac3b-7f67bb39e14d",
    "Host": "api.propublica.org",
    "Accept-Encoding": "gzip, deflate",
    "Connection": "keep-alive",
    "cache-control": "no-cache",
}

response = requests.request("GET", url, headers=headers)
response = response.json()

for i in response["results"][0]["members"]:
    district = i["district"]
    if i["district"] == "At-Large":
        district = "0"

    query = db.insert(congressmen).values(
        lastname=i["last_name"],
        firstname=i["first_name"],
        party=partyDict[i["party"]],
        state=i["state"],
        district=int(district),
        twitter=i["twitter_account"],
        lastsession=2018,
    )
    ResultProxy = connection.execute(query)
