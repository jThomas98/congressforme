import sqlalchemy as db

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
districts = db.Table("districts", metadata, autoload=True, autoload_with=engine)
congressmen = db.Table("congressmen", metadata, autoload=True, autoload_with=engine)
# query = db.select([congressmen])

# ResultProxy = connection.execute(query)
# ResultSet = ResultProxy.fetchall()
# for congressman in ResultSet:
#     state = congressman[3]
#     district = congressman[4]
#     id =congressman[9]
#     query = db.update(districts).values(congressid = id)
#     query = query.where(db.and_(districts.columns.state == state, districts.columns.number == district))
#     results = connection.execute(query)
#     print("updated " + str(id))

query = db.select([districts])

ResultProxy = connection.execute(query)
ResultSet = ResultProxy.fetchall()
for district in ResultSet:
    id = district[4]
    query = db.update(congressmen).values(districtid=district[0])
    query = query.where(congressmen.columns.congressid == id)
    results = connection.execute(query)
    print(id)
