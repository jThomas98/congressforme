import json
import datetime
import sqlalchemy as db
import requests

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
congress = db.Table("congressmen", metadata, autoload=True, autoload_with=engine)

query = db.select(
    [congress.columns.congressid, congress.columns.lastname, congress.columns.firstname]
)
Proxy = connection.execute(query)
ResultSet = Proxy.fetchall()
for d in ResultSet:
    url = (
        "https://en.wikipedia.org/w/api.php?action=query&titles="
        + d[2]
        + "_"
        + d[1]
        + "&prop=pageimages&format=json&pithumbsize=100"
    )
    response = requests.request("GET", url)
    response = response.json()
    try:
        pic = response["query"]["pages"][list(response["query"]["pages"].keys())[0]][
            "thumbnail"
        ]["source"]
    except KeyError:
        pic = "none"
    print(pic)
    query = congress.update().values(pic=pic).where(congress.columns.congressid == d[0])
    connection.execute(query)
