import json
import datetime
import sqlalchemy as db
import requests

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
districts = db.Table("districts", metadata, autoload=True, autoload_with=engine)

states = {
    "AK": "Alaska",
    "AL": "Alabama",
    "AR": "Arkansas",
    "AS": "American Samoa",
    "AZ": "Arizona",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DC": "District of Columbia",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "GU": "Guam",
    "HI": "Hawaii",
    "IA": "Iowa",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "MA": "Massachusetts",
    "MD": "Maryland",
    "ME": "Maine",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MO": "Missouri",
    "MP": "Northern Mariana Islands",
    "MS": "Mississippi",
    "MT": "Montana",
    "NA": "National",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "NE": "Nebraska",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NV": "Nevada",
    "NY": "New York",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "PR": "Puerto Rico",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VA": "Virginia",
    "VI": "Virgin Islands",
    "VT": "Vermont",
    "WA": "Washington",
    "WI": "Wisconsin",
    "WV": "West Virginia",
    "WY": "Wyoming",
}


def pretty_number(num):
    if num == 0:
        return "at-large"
    if num % 100 > 10 and num % 100 < 20:
        return str(num) + "th"
    elif num % 10 == 1:
        return str(num) + "st"
    elif num % 10 == 2:
        return str(num) + "nd"
    elif num % 10 == 3:
        return str(num) + "rd"
    else:
        return str(num) + "th"


query = db.select([districts.columns.state, districts._columns.number])
Proxy = connection.execute(query)
ResultSet = Proxy.fetchall()
for d in ResultSet:
    # url = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles="+states[d[0]]+"%27s_"+pretty_number(d[1])+"_congressional_district"
    url = (
        "https://en.wikipedia.org/w/api.php?action=query&titles="
        + states[d[0]]
        + "%27s_"
        + pretty_number(d[1])
        + "_congressional_district"
        + "&prop=pageimages&format=json&pithumbsize=100"
    )
    response = requests.request("GET", url)
    response = response.json()
    try:
        pic = response["query"]["pages"][list(response["query"]["pages"].keys())[0]][
            "thumbnail"
        ]["source"]
    except KeyError:
        pic = "none"
    else:
        print(pic)
    query = (
        districts.update()
        .values(pic=pic)
        .where(
            db.and_(districts.columns.state == d[0], districts.columns.number == d[1])
        )
    )
    connection.execute(query)
