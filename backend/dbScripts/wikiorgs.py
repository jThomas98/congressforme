import json
import datetime
import sqlalchemy as db
import requests

engine = db.create_engine(
    "postgresql+psycopg2://congress:BruhM0m3nt@database-1.ct0wj2eyoiqt.us-east-1.rds.amazonaws.com:5432/congressforme"
)
connection = engine.connect()
metadata = db.MetaData()
donors = db.Table("donors", metadata, autoload=True, autoload_with=engine)

query = db.select([donors.columns.donorid, donors.columns.name])
Proxy = connection.execute(query)
ResultSet = Proxy.fetchall()
for d in ResultSet:
    url = (
        "https://en.wikipedia.org/w/api.php?action=query&titles="
        + d[1]
        + "&prop=pageimages&format=json&pithumbsize=100"
    )
    response = requests.request("GET", url)
    response = response.json()
    try:
        pic = response["query"]["pages"][list(response["query"]["pages"].keys())[0]][
            "thumbnail"
        ]["source"]
    except KeyError:
        pic = "none"
    print(pic)
    query = donors.update().values(pic=pic).where(donors.columns.donorid == d[0])
    connection.execute(query)
