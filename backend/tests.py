# project/test_basic.py


import os
import unittest

from app import app


TEST_DB = "test.db"


class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app.config["TESTING"] = True
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        pass

    ###############
    #### tests ####
    ###############

    def test_main_page(self):
        response = self.app.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 404)

    def test_congressmen_page(self):
        response = self.app.get("/api/congressmen", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)
        self.assertEqual(json["objects"][0]["congressid"], 11)
        self.assertEqual(json["objects"][0]["crp_id"], "N00036633")

    def test_congressman_id(self):
        response = self.app.get("/api/congressmen/11", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertEqual(json["congressid"], 11)

    def test_congressman_crp_id(self):
        response = self.app.get("/api/congressmen/11", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertEqual(json["crp_id"], "N00036633")

    def test_donors_page_id(self):
        response = self.app.get("/api/donors", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)
        self.assertEqual(json["objects"][0]["donorid"], 1)
        self.assertEqual(json["objects"][0]["name"], "Democracy Engine")

    def test_donors_page_name(self):
        response = self.app.get("/api/donors", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)
        self.assertEqual(json["objects"][0]["name"], "Democracy Engine")

    def test_donor(self):
        response = self.app.get("/api/donors/1", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertEqual(json["donorid"], 1)
        self.assertEqual(json["name"], "Democracy Engine")

    def test_districts_page_status(self):
        response = self.app.get("/api/districts", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_districts_page_values(self):
        response = self.app.get("/api/districts", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)
        self.assertEqual(json["objects"][0]["districtid"], 1)
        self.assertEqual(
            json["objects"][0]["name"], "Alabama's 4th congressional district"
        )

    

    def test_districts_page(self):
        response = self.app.get("/api/districts", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)
        self.assertEqual(json["objects"][0]["districtid"], 1)
        self.assertEqual(
            json["objects"][0]["name"], "Alabama's 4th congressional district"
        )

    def test_district_id(self):
        response = self.app.get("/api/districts/1", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertEqual(json["districtid"], 1)

    def test_district_name(self):
        response = self.app.get("/api/districts/1", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertEqual(json["districtid"], 1)


if __name__ == "__main__":
    unittest.main()
