module.exports = {
  apps : [{
    name       : "congressforme-client",
    script     : "./front/node_modules/react-scripts/bin/react-scripts.js",
    watch       : true,
    env: {
      "NODE_ENV": "development",
    },
    env_production : {
      "NODE_ENV": "production"
    }
  }]
}
