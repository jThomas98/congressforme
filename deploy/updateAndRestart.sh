#!/bin/bash

# any future command that fails will exit the script
set -e

# kill running docker containers
#docker stop backend || true && docker rm backend || true
#docker stop front || true && docker rm front || true
#docker rm $(docker ps -a -q) || true

# move to the repo
sudo su
cd /srv/www/congressfor.me/congressforme
docker-compose down
cd ..

# Install newest repo
rm -rf congressforme
git clone https://gitlab.com/jThomas98/congressforme.git

# rebuild it
cd congressforme
cd front
npm install
npm run-script build
cd ..
docker-compose build
docker-compose up -d
#cd ..

# stop the previous pm2
#pm2 kill
#npm remove pm2 -g

#npm install pm2 -g
#pm2 start congressforme.config.js

# starting pm2 daemon
#pm2 status

#restart nginx
#systemctl restart nginx
