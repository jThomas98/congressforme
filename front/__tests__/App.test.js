import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai'
import Adapter from "enzyme-adapter-react-16"
import Districts from '../src/views/Districts';
import App from '../src/App';
import Representatives from '../src/views/Representatives';
import Donors from '../src/views/Donors';

configure({ adapter: new Adapter() });

// Renders app 
// it('renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });


describe('District', () => {

  it('Counts the number of h1 elements there are', () => {
    const wrapper = shallow(<Districts />);
    expect(wrapper.find('h1').length).to.equal(1);
  });

  it('Correctly renders districts title', () => {
    const wrapper = shallow(<Districts />);
    expect(wrapper.find('h1').text()).equal("Districts")
  });

  it('Looks for the loading elements screen ', () => {
    const wrapper = shallow(<Districts />);
    expect(wrapper.find('h2').length).to.equal(1);
  });

  it('Correctly renders loading elements screen', () => {
    const wrapper = shallow(<Districts />);
    expect(wrapper.find('h2').text()).equal("Loading districts...")
  });
});

describe('Representatives', () => {

  it('Counts the number of h1 elements there are', () => {
    const wrapper = shallow(<Representatives />);
    expect(wrapper.find('h1').length).to.equal(1);
  });

  it('Correctly renders Representatives title', () => {
    const wrapper = shallow(<Representatives />);
    expect(wrapper.find('h1').text()).equal("Representatives")
  });

  it('Correctly renders loading elements screen', () => {
    const wrapper = shallow(<Representatives />);
    expect(wrapper.find('h2').text()).equal("Loading congressmen...")
  });
});

describe('Donors', () => {

  it('Counts the number of h1 elements there are', () => {
    const wrapper = shallow(<Donors />);
    expect(wrapper.find('h1').length).to.equal(1);
  });

  it('Correctly renders Representatives title', () => {
    const wrapper = shallow(<Donors />);
    expect(wrapper.find('h1').text()).equal("Donors")
  });

  it('Correctly renders loading elements screen', () => {
    const wrapper = shallow(<Donors />);
    expect(wrapper.find('h2').text()).equal("Loading Donors...")
  });
});

