import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class IntegrationTests(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('./chromedriver')

    def test_title(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/")
        self.assertEqual(driver.title, "CongressForMe")

    def test_home_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_representatives_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Representatives")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_representative_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Representatives/16")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_districts_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Districts")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_district_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Districts/1")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_donors_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Donors")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_donor_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Donors/1")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_about_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/About")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)
    
    def test_search_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/Search")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def test_404_nav(self):
        driver = self.driver
        driver.get("https://www.congressfor.me/404itup")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()