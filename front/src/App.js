import React, { Component } from "react";
import { Link } from 'react-router-dom';
import "./App.css";
import { Nav, Navbar, Button, FormControl, Form } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap'

class App extends Component {
  constructor(props) {
    super(props)

    //set the default search text
    this.state = {
      searchText: "",
      redirect: false
    }
    //Bbinds handleInput
    this.handleInput = this.handleInput.bind(this)
  }

   //Method to handle typing in search bar
  handleInput(event) {
    this.setState({
      searchText: event.target.value
    })
  }

  //Method to handle submit on search bar
  handleSubmit(event) {
    event.preventDefault()
  }

  render() {
    return (
      <div className="AppCon">
        {/* Code for Navbar */}
        <Navbar fluid="true" collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" justify-content-between="true">
          <Navbar.Brand href="/">
            CongressForMe
              </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link eventKey="1" as={Link} to="/Representatives">Representatives</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="2" as={Link} to="/Districts">Districts</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="3" as={Link} to="/Donors">Donors</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="4" as={Link} to="/Visualizations">Visualizations</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="5" as={Link} to="/About">About</Nav.Link>
              </Nav.Item>
            </Nav>
            {/* Code for seach bar */}
            <Form onSubmit={this.handleSubmit.bind(this)} inline>
              <FormControl
                onChange={this.handleInput}
                value={this.state.searchText}
                type="text"
                placeholder="Search"
                className="mr-sm-2" />

              <LinkContainer to={{ pathname: "/Search", state: { searchText: this.state.searchText } }} >
                <Button type="submit"
                  className="btn btn-info">
                  Search
                </Button>
              </LinkContainer>
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default App;
