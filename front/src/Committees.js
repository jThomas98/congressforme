const committees = new Map([
    ["3", "Committee on Administration"],
    ["4", "Committee on Agriculture"],
    ["5", "Committee on Appropriations"],
    ["6", "Committee on Armed Services"],
    ["7", "Committee on Education and Labor"],
    ["8", "Committee on Energy and Commerce"],
    ["9", "Committee on Ethics"],
    ["10", "Committee on Financial Services"],
    ["11", "Committee on Foreign Affairs"],
    ["12", "Committee on Homeland Security"],
    ["13", "Committee on Natural Resources"],
    ["14", "Committee on Oversight and Reform"],
    ["15", "Committee on Rules"],
    ["16", "Committee on Science, Space, and Technology"],
    ["17", "Committee on Small Business"],
    ["18", "Committee on the Budget"],
    ["19", "Committee on the Judiciary"],
    ["20", "Committee on Transportation and Infrastructure"],
    ["21", "Committee on Veterans' Affairs"],
    ["22", "Committee on Ways and Means"],
    ["23", "Permanent Select Committee on Intelligence"],
    ["24", "Select Committee on the Climate Crisis"],
    ["25", "Select Committee on the Modernization of Congress"],
]);

function getCommittee(id) {
    const stringId = id + "";
    return committees.get(stringId);
}

export default function getCommittees(congresscomittee) {
    let str = "";
    // assuming congresscommittee is an array of objects
    for (let i = 0; i < congresscomittee.length; ++i) {
        let id = congresscomittee[i].committeeid;
        str += getCommittee(id);
        if (i !== congresscomittee.length - 1) {
            str += ", ";
        }
    }
    return str;
}