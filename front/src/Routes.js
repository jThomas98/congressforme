import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import Home from "./views/Home";
import Representatives from "./views/Representatives";
import Districts from "./views/Districts";
import DistrictModel from "./views/DistrictModel";
import Donors from "./views/Donors";
import About from "./views/About";
import Visualizations from "./views/Visualizations";
import Search from "./views/Search";
import NotFound from "./views/NotFound";
import RepresentativeModel from './views/RepresentativeModel';
import DonorModel from './views/DonorModel';

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/Representatives/:congressid" component={RepresentativeModel} />
                <Route exact path="/Representatives" component={Representatives} />
                <Route exact path="/Districts" component={Districts} />
                <Route exact path="/Districts/:district" component={DistrictModel} />
                <Route exact path="/Donors" component={Donors} />
                <Route exact path="/Donors/:donorid" component={DonorModel} />
                <Route exact path="/Visualizations" component={Visualizations} />
                <Route exact path="/About" component={About} />
                <Route exact path="/Search" component={Search} />
                <Route component={NotFound} />
            </Switch>
        );
    }
}
