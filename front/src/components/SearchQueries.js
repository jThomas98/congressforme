// Returns query for representatives given array of searchTerms and a page number
export function getRepsSearchQuery(searchTerms, repsCurrPage) {
  let repsPage = repsCurrPage + 1;
  let repApi = "https://www.congressfor.me/api/congressmen?q={\"filters\":[{\"or\":["
  for (let i = 0; i < searchTerms.length; ++i) {
    let currSearchText = searchTerms[i]
    currSearchText = currSearchText.charAt(0).toUpperCase() + currSearchText.slice(1)
    repApi += (isNaN(currSearchText)) ?
      "{\"name\":\"fullname\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"firstname\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"lastname\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"phone\",\"op\":\"like\",\"val\":\"" + currSearchText + "\"}," +
      "{\"name\":\"state\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"party\",\"op\":\"like\",\"val\":\"" + currSearchText + "\"},"
      :
      "{\"name\":\"district\",\"op\":\"eq\",\"val\":\"" + currSearchText + "\"}," +
      "{\"name\":\"lastsession\",\"op\":\"eq\",\"val\":\"" + currSearchText + "\"},"
  }
  repApi = repApi.substring(0, repApi.length - 1)
  repApi += "]}]}&page=" + repsPage;
  return repApi
}

// Returns query for districts given array of searchTerms and a page number
export function getDistrictsSearchQuery(searchTerms, districtsCurrPage) {
  let districtsPage = districtsCurrPage + 1;
  let districtApi = "https://www.congressfor.me/api/districts?q={\"filters\":[{\"or\":["
  for (let i = 0; i < searchTerms.length; ++i) {
    let currSearchText = searchTerms[i]
    districtApi += (isNaN(currSearchText)) ?
      "{\"name\":\"name\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"election_date\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"party\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"wiki\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"}," +
      "{\"name\":\"state\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"},"
      :
      "{\"name\":\"number\",\"op\":\"eq\",\"val\":\"" + currSearchText + "\"}," +
      "{\"name\":\"party\",\"op\":\"ilike\",\"val\":\"%" + currSearchText + "%\"},"

  }
  districtApi = districtApi.substring(0, districtApi.length - 1)
  districtApi += "]}]}&page=" + districtsPage;
  return districtApi
}

// Returns query for donors given array of searchTerms and a page number
export function getDonorsSearchQuery(searchTerms, donorsCurrPage) {
  let donorsPage = donorsCurrPage + 1;
  let donorsApi = "https://www.congressfor.me/api/donors?q={\"filters\":[{\"or\":["
  for (let i = 0; i < searchTerms.length; ++i) {
    let currSearchText = searchTerms[i]
    donorsApi += (isNaN(currSearchText)) ?
      "{\"name\":\"name\",\"op\":\"ilike\",\"val\":\"%" + currSearchText +
      "%\"}," :
      "{\"name\":\"name\",\"op\":\"ilike\",\"val\":\"" + currSearchText +
      "\"},{\"name\":\"cycle\",\"op\":\"eq\",\"val\":\"}" + currSearchText +
      "\"},{\"name\":\"dem_contributions\",\"op\":\"eq\",\"val\":\"" + currSearchText +
      "\"}, {\"name\":\"rep_contributions\",\"op\":\"eq\",\"val\":\"" + currSearchText +
      "\"},"
  }
  donorsApi = donorsApi.substring(0, donorsApi.length - 1)
  donorsApi += "]}]}&page=" + donorsPage;
  return donorsApi
}