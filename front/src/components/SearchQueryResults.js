import axios from 'axios';

// Returns query results and number of pages for representatives given search query
export async function getRepsSearchQueryResult(searchQuery) {
  const headers = { 'Access-Control-Allow-Origin': '*' };
  let reps = []
  let pages;
  await axios.get(searchQuery, headers).then(function (repsResponse) {
    const objects = repsResponse.data.objects;
    const totalPages = repsResponse.data.total_pages;
    for (let i = 0; i < objects.length; ++i) {
      const rep = objects[i];
      reps.push(rep);
    }
    pages = totalPages
  });
  return { reps, pages }
}

// Returns query results and number of pages for districts given search query
export async function getDistrictsSearchQueryResult(searchQuery) {
  const headers = { 'Access-Control-Allow-Origin': '*' };
  let districts = []
  let pages;
  await axios.get(searchQuery, headers).then(function (districtsResponse) {
    const objects = districtsResponse.data.objects;
    const totalPages = districtsResponse.data.total_pages;
    for (let i = 0; i < objects.length; ++i) {
      const district = objects[i];
      districts.push(district);
    }
    pages = totalPages
  });

  return { districts, pages }
}

export async function getDonorsSearchQueryResult(searchQuery) {
  const headers = { 'Access-Control-Allow-Origin': '*' };
  let donors = []
  let pages;
  await axios.get(searchQuery, headers).then(function (donorsResponse) {
    const objects = (donorsResponse.data).objects;
    const totalPages = donorsResponse.data.total_pages;
    for (let i = 0; i < objects.length; ++i) {
      const donor = objects[i];
      donors.push(donor);
    }
    pages = totalPages
  });
  return { donors, pages }
}
