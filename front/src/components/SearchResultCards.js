import React from "react";
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import Highlighter from "react-highlight-words";

export function renderRepSearchResultCard(rep, index, searchTerms) {
  const r = rep;
  const linkString = "/Representatives/" + r.congressid;
  const fullLink = "https://www.congressfor.me" + linkString
  const repName = r.firstname + " " + r.lastname
  let validAttributes = ["lastname", "firstname", "phone", "district", "lastsession", "state", "party"]
  let cardText = createCardText(r, validAttributes, searchTerms, true)

  return (
    <tr key={index}>
      <td>
        <Card>
          <Card.Header as="h5">{<Link to={{ pathname: linkString }}>{repName}</Link>}</Card.Header>
          <Card.Body>
            <Card.Text>{fullLink}</Card.Text>
            <Card.Text><Highlighter searchWords={searchTerms} textToHighlight={cardText} /></Card.Text>
          </Card.Body>
        </Card>
      </td>
    </tr>
  )
}

export function renderDistrictSearchResultCard(district, index, searchTerms) {
  const d = district;
  const linkString = "/Districts/" + d.districtid;
  const fullLink = "https://www.congressfor.me" + linkString
  const districtName = d.name
  const validAttributes = ["name", "state", "party", "number", "election_date", "wiki"]
  const cardText = createCardText(d, validAttributes, searchTerms, true)

  return (
    <tr key={index}>
      <td>
        <Card>
          <Card.Header as="h5">{<Link to={{ pathname: linkString }}>{districtName}</Link>}</Card.Header>
          <Card.Body>
            <Card.Text>{fullLink}</Card.Text>
            <Card.Text><Highlighter searchWords={searchTerms} textToHighlight={cardText} /></Card.Text>
          </Card.Body>
        </Card>
      </td>
    </tr>
  )
}

export function renderDonorSearchResultCard(donor, index, searchTerms) {
  const d = donor;
  const linkString = "/Donors/" + d.donorid;
  const fullLink = "https://www.congressfor.me" + linkString
  const donorName = d.name
  const validAttributes = ["nametext", "cycle", "dem_contributions", "rep_contributions"]
  const cardText = createCardText(d, validAttributes, searchTerms, true)

  return (
    <tr key={index}>
      <td>
        <Card>
          <Card.Header as="h5">{<Link to={{ pathname: linkString }}>{donorName}</Link>}</Card.Header>
          <Card.Body>
            <Card.Text>{fullLink}</Card.Text>
            <Card.Text><Highlighter searchWords={searchTerms} textToHighlight={cardText} /></Card.Text>
          </Card.Body>
        </Card>
      </td>
    </tr>
  )
}

function createCardText(model, validAttributes, searchTerms, lowerCase) {
  let cardText = ""
  for (let attribute in model) {
    let data = "" + model[attribute]
    let dataLower;
    if (lowerCase) {
      dataLower = data.toLowerCase()
    }
    if (validAttributes.includes(attribute)) {
      if (attribute === "nametext") {
        attribute = "name"
      }
      for (let i = 0; i < searchTerms.length; ++i) {
        let searchTerm = searchTerms[i]
        let currData = data
        if (lowerCase) {
          currData = dataLower
        }
        if (currData.includes(searchTerm.toLowerCase())) {
          let attributeUpper = attribute.charAt(0).toUpperCase() + attribute.slice(1)
          let dataUpper = data.charAt(0).toUpperCase() + data.slice(1)
          cardText += attributeUpper + ": " + dataUpper + " ... "
        }
      }
    }
  }
  return cardText.substring(0, cardText.length - 5)
}
