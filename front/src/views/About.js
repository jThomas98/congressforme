import React from 'react';
import './About.css';
import { Container, Col } from 'react-bootstrap';
import JasonPicture from '../assets/jason.jpeg';
import RyanPicture from '../assets/ryan.jpeg';
import DarriusPicture from '../assets/darrius.jpg';
import PranavPicture from '../assets/pranav.jpeg';
import JorgePicture from '../assets/jorge.jpeg';

//import logos
import Sqlalchemy_logo from '../assets/SQLachemy_logo.jpeg';
import Selenium_logo from '../assets/Selenium_logo.png';
import React_logo from '../assets/react_logo.png';
import Postman_logo from '../assets/postman-icon.png';
import PostgreSQL_logo from '../assets/postgreSQL_logo.png';
import plantUML_logo from '../assets/plantUML_logo.png';
import Mocha_logo from '../assets/mocho_logo.png';
import FlaskRess_logo from '../assets/FlaskRestless_logo.png';
import Flask_logo from '../assets/flask_logo.png';
import Docker_logo from '../assets/docker_logo.png';
import ReactBootstrap_logo from '../assets/ReactBoot_logo.png';
import AWS_logo from '../assets/aws_logo.png';


import { Card, ListGroup, ListGroupItem, CardColumns, CardDeck } from 'react-bootstrap';

var members = {
    "Pranav Neravetla": {
        name: "Pranav Neravetla",
        commits: 0,
        issues: 0,
        unitTests: 0,
        username: "pranavneravetla"
    },
    "Ryan Menghani": {
        name: "Ryan Menghani",
        commits: 0,
        issues: 0,
        unitTests: 0,
        username: "menghaniryan"
    },
    "Jorge Garcia": {
        name: "Jorge Garcia",
        commits: 0,
        issues: 0,
        unitTests: 0,
        username: "jgarcia525"
    },
    "Jason Thomas": {
        name: "Jason Thomas",
        commits: 0,
        issues: 0,
        unitTests: 35,
        username: "jThomas98"
    },
    "Darrius Anderson": {
        name: "Darrius Anderson",
        commits: 0,
        issues: 0,
        unitTests: 0,
        username: "darrius35"
    }
};
var totals = {
    commits: 0,
    issues: 0,
    unitTests: 35

};
class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        };
    }
    componentDidMount() {
        fetch("https://gitlab.com/api/v4/projects/14477830/repository/contributors")
            .then(res => res.json())
            .then(res => {
                for (let i = 0; i < res.length; i++) {
                    const currUser = res[i].name;
                    if (members[currUser]) {
                        const currCommits = res[i].commits;
                        members[currUser].commits += currCommits;
                        totals.commits += currCommits;
                        const state = {}
                        state.commits = members[currUser].commits;
                        this.setState(state);
                    }
                }
            })
        Object.keys(members).forEach(member => {
            const username = members[member].username;
            fetch("https://gitlab.com/api/v4/projects/14477830/issues_statistics?assignee_username=" + username)
                .then(res => res.json())
                .then(res => {
                    const currIssues = res.statistics.counts.all;
                    members[member].issues = currIssues;
                    totals.issues += currIssues;
                    const state = {}
                    state.issues = members[member].issues;
                    this.setState(state);
                })
        });
    }
    render() {
        document.body.style.backgroundImage = "none";
        return (
            <Container>
                <div className="">
                    <h1 style={{ textAlign: "center", margin: "25px" }}>About Us</h1>
                    <p style={{ textAlign: "center", margin: "25px", fontWeight: "bold" }}>We built this website to make it easier for the average person to find information about their representatives as well as find out where that
                 representative is getting their support.  </p>
                    <Container>
                        <hr />
                        <CardDeck className="justify-content-center">
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card class="text-center" style={{ width: '300px' }}>
                                    <Card.Img variant="top" src={RyanPicture} style={{ height: "18rem", objectFit: "cover" }} />
                                    <Card.Body>
                                        <Card.Title>Ryan Menghani</Card.Title>
                                        <Card.Subtitle>
                                            Frontend
                                </Card.Subtitle>
                                        <Card.Text>
                                            Junior computer science student who likes to run and play Super Smash Bros.
                                </Card.Text>
                                    </Card.Body>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members["Ryan Menghani"].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members["Ryan Menghani"].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members["Ryan Menghani"].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card class="text-center" style={{ width: '300px' }}>
                                    <Card.Img variant="top" src={JorgePicture} style={{ height: "18rem", objectFit: "cover" }} />
                                    <Card.Body>
                                        <Card.Title>Jorge Garcia</Card.Title>
                                        <Card.Subtitle>
                                            Full stack
                                </Card.Subtitle>
                                        <Card.Text>
                                            I am a 4th year Computer Science major, and I participate in SHPE, HACS, and the Texas Running Club
                                </Card.Text>
                                    </Card.Body>
                                    <ListGroup className="list-group-flush" >
                                        <ListGroupItem><b>Commits: </b>{members["Jorge Garcia"].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members["Jorge Garcia"].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members["Jorge Garcia"].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card class="text-center" style={{ width: '300px' }}>
                                    <Card.Img variant="top" src={PranavPicture} style={{ height: "18rem", objectFit: "cover" }} />
                                    <Card.Body>
                                        <Card.Title>Pranav Neravetla</Card.Title>
                                        <Card.Subtitle>
                                            Frontend
                                </Card.Subtitle>
                                        <Card.Text>
                                            I thought I was done spending nights in the GDC after OS but here I am doing it again.
                                </Card.Text>
                                    </Card.Body>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members["Pranav Neravetla"].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members["Pranav Neravetla"].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members["Pranav Neravetla"].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                        </CardDeck>

                        <hr style={{ visibility: "hidden" }} />
                        <CardDeck className="justify-content-center">
                            <Col className="justify-content-center" md="12" lg="4">

                                <Card style={{ width: '300px' }}>
                                    <Card.Img variant="top" src={DarriusPicture} style={{ height: "18rem", objectFit: "cover" }} />
                                    <Card.Body>
                                        <Card.Title>Darrius Anderson</Card.Title>
                                        <Card.Subtitle>
                                            Frontend
                                </Card.Subtitle>
                                        <Card.Text>
                                            I am a third year CS student at UT who enjoys a good challenge.
                                </Card.Text>
                                    </Card.Body>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members["Darrius Anderson"].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members["Darrius Anderson"].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members["Darrius Anderson"].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card style={{ width: '300px' }}>
                                    <Card.Img variant="top" src={JasonPicture} style={{ height: "18rem", objectFit: "cover" }} />
                                    <Card.Body>
                                        <Card.Title>Jason Thomas</Card.Title>
                                        <Card.Subtitle>
                                            Backend
                                </Card.Subtitle>
                                        <Card.Text>
                                            Senior computer science major. Fisherman. Dog lover. Baller.
                                </Card.Text>
                                    </Card.Body>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members["Jason Thomas"].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members["Jason Thomas"].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members["Jason Thomas"].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                        </CardDeck>
                        <hr />
                        <h2>
                            Links
                        </h2>
                        <CardDeck className="justify-content-center">
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card className="text-center" style={{ width: '300px' }}>
                                    <Card.Header as="h5">Gitlab</Card.Header>
                                    <Card.Body>
                                        <Card.Title>Team Totals:</Card.Title>
                                        <Card.Text>
                                            <b>Commits: </b>{totals.commits}
                                        </Card.Text>
                                        <Card.Text>
                                            <b>Issues: </b>{totals.issues}
                                        </Card.Text>
                                        <Card.Text>
                                            <b>Unit Tests: </b>{totals.unitTests}
                                        </Card.Text>
                                        <Card.Text>
                                            <a href="https://gitlab.com/jThomas98/congressforme" class="btn btn-dark">Gitlab Repository</a>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card className="text-center" style={{ width: '300px' }}>
                                    <Card.Header as="h5">Postman</Card.Header>
                                    <Card.Body>
                                        <Card.Img variant="top" src={Postman_logo} style={{ width: "60%", height: "9.5rem", objectFit: "cover" }} />

                                        <Card.Text>
                                            <a href="https://documenter.getpostman.com/view/8954632/SVtR1VLQ" class="btn btn-dark">Postman API</a>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col className="justify-content-center" md="12" lg="4">
                                <Card className="text-center" style={{ width: '300px', height: "17.5rem" }}>
                                    <Card.Header as="h5">API Links</Card.Header>
                                    <Card.Body>
                                        <hr style={{ visibility: "hidden" }} />
                                        <Card.Text>
                                            <a href="https://www.opensecrets.org/open-data/api" class="">Open Secrets API</a>
                                        </Card.Text>
                                        <hr style={{ visibility: "hidden" }} />
                                        <Card.Text>
                                            <a href="https://developers.google.com/civic-information" class="">Google Civic-Information API</a>
                                        </Card.Text>
                                        <hr style={{ visibility: "hidden" }} />
                                        <Card.Text>
                                            <a href="https://projects.propublica.org/api-docs/congress-api/" class="">ProPublica API</a>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </CardDeck>
                    </Container>
                    <hr />
                    <h2>
                        Tools
                    </h2>
                    <CardColumns>
                        <Card className="text-center">
                            <a href="https://www.sqlalchemy.org">
                                <Card.Img variant="top" src={Sqlalchemy_logo} style={{ width: "200px", height: "4rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Sqlalchemy</Card.Title>
                                <Card.Text>
                                    Used to access SQL database
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://www.seleniumhq.org">
                                <Card.Img variant="top" src={Selenium_logo} style={{ width: "180px", height: "10rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Selenium</Card.Title>
                                <Card.Text>
                                    Integration testing
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://reactjs.org">
                                <Card.Img variant="top" src={React_logo} style={{ width: "200px", height: "8rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>React</Card.Title>
                                <Card.Text>
                                    Front-end framework
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://www.getpostman.com">
                                <Card.Img variant="top" src={Postman_logo} style={{ width: "170px", height: "10rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Postman</Card.Title>
                                <Card.Text>
                                    API design
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://www.postgresql.org">
                                <Card.Img variant="top" src={PostgreSQL_logo} style={{ width: "200px", height: "13rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>PostgreSQL</Card.Title>
                                <Card.Text>
                                    Database
                        </Card.Text>
                            </Card.Body>

                        </Card>
                        <Card className="text-center">
                            <a href="http://plantuml.com/">
                                <Card.Img variant="top" src={plantUML_logo} style={{ width: "200px", height: "12rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>plantUML</Card.Title>
                                <Card.Text>
                                    Visualizing the database
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://mochajs.org">
                                <Card.Img variant="top" src={Mocha_logo} style={{ width: "200px", height: "12rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Mocha</Card.Title>
                                <Card.Text>
                                    Frontend testing
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://flask-restless.readthedocs.io/en/stable/">
                                <Card.Img variant="top" src={FlaskRess_logo} style={{ width: "200px", height: "12rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Flask Restless</Card.Title>
                                <Card.Text>
                                    Used to create restless API
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://palletsprojects.com/p/flask/">
                                <Card.Img variant="top" src={Flask_logo} style={{ width: "200px", height: "10rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Flask</Card.Title>
                                <Card.Text>
                                    Back-End framework
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://www.docker.com/">
                                <Card.Img variant="top" src={Docker_logo} style={{ width: "200px", height: "10rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>Docker</Card.Title>
                                <Card.Text>
                                    For running frontend and backend on EC2
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://react-bootstrap.github.io/">
                                <Card.Img variant="top" src={ReactBootstrap_logo} style={{ width: "170px", height: "10.1rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>React Bootstrap</Card.Title>
                                <Card.Text>
                                    HTML and CSS framework
                        </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <a href="https://aws.amazon.com/">
                                <Card.Img variant="top" src={AWS_logo} style={{ width: "180px", height: "7rem", objectFit: "cover" }} />
                            </a>
                            <Card.Body>
                                <Card.Title>AWS</Card.Title>
                                <Card.Text>
                                    Web hosting
                        </Card.Text>
                            </Card.Body>
                        </Card>
                    </CardColumns>
                </ div>
            </Container>
        );
    }
}
export default About;