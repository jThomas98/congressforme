import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';

export default class BarGraph extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stateReps: [],
      loaded: false
    };

    this.createBarGraph = this.createBarGraph.bind(this);
    this.fetchStateReps = this.fetchStateReps.bind(this);
  }

  componentDidMount() {
    this.fetchStateReps();
    if (this.state.loaded) {
      console.log("finished loading... component did mount");
      this.createBarGraph();
    }
  }

  componentDidUpdate() {
    if (this.state.loaded) {
      console.log("finshed loading... component did update");
      this.createBarGraph();
    }
  }


  fetchStateReps() {
    const headers = { 'Access-Control-Allow-Origin': '*' };

    const api = 'https://www.congressfor.me/api/state_reps';

    // get all of the representatives for the state, push them to a promises array 
    const promises = [];

    // numPages and numResultsPerPage are known from looking at the data, hardcoded in
    let numPages = 6;
    for (let curPage = 1; curPage <= numPages; ++curPage) {
      const pagedApi = api + "?page=" + curPage;
      promises.push(axios.get(pagedApi, headers));
    }

    // myStateReps stores the states and their rep counts as objects, and is set to stateReps 
    // at the end of the Promise
    const myStateReps = []

    Promise.all(promises).then((results) => {
      // the results of all of the requests are stored in the results variable, which is an array
      for (let resultIdx = 0; resultIdx < results.length; ++resultIdx) {
        const data = results[resultIdx].data;
        const objects = data.objects;
        // go through each result and actually store its data
        for (let dataResultIdx = 0; dataResultIdx < objects.length; ++dataResultIdx) {
          const object = objects[dataResultIdx];
          myStateReps.push(object);
        }
      }
      this.setState({ stateReps: myStateReps, loaded: true });
    });

  }

  createBarGraph() {
    let margin = ({ top: 20, right: 0, bottom: 30, left: 40 });
    let width = window.screen.width;
    let height = 650;

    const stateReps = this.state.stateReps;

    let x = d3.scaleBand()
      .domain(stateReps.map(d => d.state))
      .range([margin.left, width - margin.right])
      .padding(0.1);

    let maxCount = d3.max(stateReps, d => d.count);
    let y = d3.scaleLinear()
      .domain([maxCount, 1]).nice()
      .range([margin.top, height - margin.bottom]);

    let xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x)
        .tickSizeOuter(0));

    let yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y))
      .call(g => g.select(".domain").remove());


    const svg = d3.select("#body8");

    svg.append("g")
      .attr("fill", "rgb(219, 152, 134)")
      .selectAll("rect").data(stateReps).enter().append("rect")
      .attr("x", d => x(d.state))
      .attr("y", d => y(d.count))
      .attr("height", d => y(0) - y(d.count))
      .attr("width", x.bandwidth());

    //Create labels
    svg.selectAll("text")
      .data(stateReps)
      .enter()
      .append("text")
      .attr("class", "label")
      .attr("x", (function (d) {
        return x(d.state);
      }))
      .attr("y", function (d) {
        return y(d.count) - 14;
      })
      .attr("dy", ".75em")
      .text(d => d.count);

    svg.append("g")
      .call(xAxis);

    svg.append("g")
      .call(yAxis);


  }

  render() {
    return (
      <div>
        <h1 style={{ textAlign: "center" }}> Number of Representatives Per State </h1>
        <div>
          <svg id='body8' width="100%" height={650}></svg>
        </div>

      </div>
    );
  }
}