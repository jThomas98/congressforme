import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';
import { Library } from '@observablehq/stdlib/';

export default class BubbleChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stateMoney: [],
      loaded: false
    };

    this.createBubbleChart = this.createBubbleChart.bind(this);
    this.fetchStateMoney = this.fetchStateMoney.bind(this);
  }

  componentDidMount() {
    this.fetchStateMoney();
    if (this.state.loaded) {
      console.log("finished loading... component did mount");
      this.createBubbleChart();
    }
  }

  componentDidUpdate() {
    if (this.state.loaded) {
      console.log("finshed loading... component did update");
      this.createBubbleChart();
    }
  }


  fetchStateMoney() {
    const headers = { 'Access-Control-Allow-Origin': '*' };

    const api = 'https://www.congressfor.me/api/state_money';

    // get all of the representatives for the state, push them to a promises array 
    const promises = [];

    // numPages is known from looking at the data, hardcoded in
    let numPages = 5;
    for (let curPage = 1; curPage <= numPages; ++curPage) {
      const pagedApi = api + "?page=" + curPage;
      promises.push(axios.get(pagedApi, headers));
    }

    // myStateMoney stores the states and their money amounts as objects, and is set to stateMoney 
    // at the end of the Promise
    const myStateMoney = []

    Promise.all(promises).then((results) => {
      // the results of all of the requests are stored in the results variable, which is an array
      for (let resultIdx = 0; resultIdx < results.length; ++resultIdx) {
        const data = results[resultIdx].data;
        const objects = data.objects;
        // go through each result and actually store its data
        for (let dataResultIdx = 0; dataResultIdx < objects.length; ++dataResultIdx) {
          const object = objects[dataResultIdx];
          myStateMoney.push(object);
        }
      }
      this.setState({ stateMoney: myStateMoney, loaded: true });
    });

  }

  createBubbleChart() {

    const myLibrary = new Library();
    const DOM = myLibrary.DOM;
    const data = this.state.stateMoney;

    const color = d3.scaleOrdinal(data.map(d => d.group), d3.schemeCategory10);
    const format = d3.format(",d");

    const width = window.screen.width * .75;
    const height = 650;


    const pack = data => d3.pack()
      .size([width - 2, height - 2])
      .padding(3)(d3.hierarchy({ children: data })
        .sum(d => d.totalmoney))

    const root = pack(data);

    const svg = d3.select("#body9");

    const leaf = svg.selectAll("g")
      .data(root.leaves())
      .join("g")
      .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`);

    leaf.append("circle")
      .attr("id", d => (d.leafUid = DOM.uid("leaf")).id)
      .attr("r", d => d.r)
      .attr("fill-opacity", 0.7)
      .attr("fill", d => color(d.data.group));

    leaf.append("clipPath")
      .attr("id", d => (d.clipUid = DOM.uid("clip")).id)
      .append("use")
      .attr("xlink:href", d => d.leafUid.href);

    leaf.append("text")
      .attr("clip-path", d => d.clipUid)
      .selectAll("tspan")
      .data(d => d.data.state.split(/(?=[A-Z][^A-Z])/g))
      .join("tspan")
      .attr("x", 0)
      .attr("y", (d, i, nodes) => `${i - nodes.length / 2 + 0.8}em`)
      .text(d => d);

    leaf.append("title")
      .text(d => `${"Total Amount:"}\n${format(d.value)}`);
  }

  render() {
    return (
      <div>
        <h1 style={{ textAlign: "center" }}> Total Money Donated to Each State </h1>
        <div>
          <svg id='body9' width="100%" height={650}></svg>
        </div>

      </div>
    );
  }
}