import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';
import { Library } from '@observablehq/stdlib/';

export default class DevBubble extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stateDiseases: [],
      loaded: false
    };

    this.createBubbleChart = this.createBubbleChart.bind(this);
    this.fetchStateDiseases = this.fetchStateDiseases.bind(this);
  }

  componentDidMount() {
    this.fetchStateDiseases();
    if (this.state.loaded) {
      console.log("finished loading... component did mount");
      this.createBubbleChart();
    }
  }

  componentDidUpdate() {
    if (this.state.loaded) {
      console.log("finshed loading... component did update");
      this.createBubbleChart();
    }
  }


  fetchStateDiseases() {
    const headers = { 'Access-Control-Allow-Origin': '*' };

    const api = 'https://api.thehealthodyssey.com/api/locations';

    // get all of the representatives for the state, push them to a promises array 
    const promises = [];

    // numPages is known from looking at the data, hardcoded in
    let numPages = 1;
    for (let curPage = 1; curPage <= numPages; ++curPage) {
      const pagedApi = api + "?page=" + curPage;
      promises.push(axios.get(pagedApi, headers));
    }

    // myStateMoney stores the states and their money amounts as objects, and is set to stateMoney 
    // at the end of the Promise
    const myStateDiseases = []

    Promise.all(promises).then((results) => {
      // the results of all of the requests are stored in the results variable, which is an array
      for (let resultIdx = 0; resultIdx < results.length; ++resultIdx) {
        const data = results[resultIdx].data;
        const objects = data.objects;
        // go through each result and actually store its data
        for (let dataResultIdx = 0; dataResultIdx < objects.length; ++dataResultIdx) {
          const object = objects[dataResultIdx];
          myStateDiseases.push(object);
        }
      }
      this.setState({ stateDiseases: myStateDiseases, loaded: true });
    });

  }

  createBubbleChart() {

    const myLibrary = new Library();
    const DOM = myLibrary.DOM;
    const data = this.state.stateDiseases;

    const color = d3.scaleOrdinal(data.map(d => d.group), d3.schemeCategory10);
    const format = d3.format(",d");

    const width = window.screen.width * .60;
    const height = 650;


    const pack = data => d3.pack()
      .size([width - 2, height - 2])
      .padding(50)(d3.hierarchy({ children: data })
        .sum(d => d.number_of_diseases))

    const root = pack(data);

    const svg = d3.select("#body11");

    const leaf = svg.selectAll("g")
      .data(root.leaves())
      .join("g")
      .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`);

    leaf.append("circle")
      .attr("id", d => (d.leafUid = DOM.uid("leaf")).id)
      .attr("r", d => d.r + 10)
      .attr("fill-opacity", 0.7)
      .attr("fill", d => color(d.data.group));

    leaf.append("clipPath")
      .attr("id", d => (d.clipUid = DOM.uid("clip")).id)
      .append("use")
      .attr("xlink:href", d => d.leafUid.href);

    leaf.append("text")
      .attr("clip-path", d => d.clipUid)
      .selectAll("tspan")
      .data(d => format(d.data.number_of_diseases))
      .join("tspan")
      .attr("x", 0)
      .attr("y", (d, i, nodes) => `${i - nodes.length / 2 + 0.8}em`)
      .text(d => d);

    leaf.append("title")
      .text(d => `${"Total Diseases for " + d.data.name + ":"}\n${format(d.data.number_of_diseases)}`);
  }

  render() {
    return (
      <div>
        <h1 style={{ textAlign: "center" }}> Total Diseases by Country</h1>
        <div>
          <svg id='body11' width="100%" height={650}></svg>
        </div>

      </div>
    );
  }
}