import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';

export default class DevPieChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      regions: [],
      loaded: false
    };

    this.createPieChart = this.createPieChart.bind(this);
    this.fetchLocations = this.fetchLocations.bind(this);
  }

  componentDidMount() {
    this.fetchLocations();
    if (this.state.loaded) {
      console.log("finished loading... component did mount");
      this.createPieChart();
    }
  }

  componentDidUpdate() {
    if (this.state.loaded) {
      console.log("finshed loading... component did update");
      this.createPieChart();
    }
  }

  fetchLocations() {
    const headers = { 'Access-Control-Allow-Origin': '*' };

    const api = 'https://api.thehealthodyssey.com/api/locations';
    const my_map = new Map();
    axios.get(api, headers).then((response) => {
      const data = response.data;
      const num_results = data.num_results;
      const objects = data.objects;
      for (let objectsIdx = 0; objectsIdx < num_results; ++objectsIdx) {
        const object = objects[objectsIdx];
        const region = object.region;
        if (my_map.has(region)) {

          // update the total with the mortality rate of this state and increment the count
          const average_preventable_deaths = my_map.get(region);
          average_preventable_deaths.total += object.preventable_deaths;
          average_preventable_deaths.count += 1;
          my_map.set(region, average_preventable_deaths);
        } else {

          // create an object that keeps track of the mortality rate and the count of the number of states per region
          const average_preventable_deaths = {
            total: object.preventable_deaths,
            count: 1
          };
          console.log(my_map);
          my_map.set(region, average_preventable_deaths);
        }
      }

      // compute the averages and push them into a new regions array
      const my_regions = [];
      for (const [key, value] of my_map.entries()) {
        const my_name = key;
        const average = value.total / value.count;
        const region = {
          name: my_name,
          preventable_deaths: average
        };
        my_regions.push(region);
      };
      this.setState({ regions: my_regions, loaded: true });
    });
  }

  createPieChart() {
    const data = {
      "Africa": this.state.regions[0].preventable_deaths, "Americas": this.state.regions[1].preventable_deaths, "Eastern Mediterranean": this.state.regions[2].preventable_deaths,
      "Europe": this.state.regions[3].preventable_deaths, "South-East Asia": this.state.regions[4].preventable_deaths, "Western Pacific": this.state.regions[5].preventable_deaths
    };


    var width = window.screen.width * .75
    var height = 450
    var margin = 40

    // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
    var radius = Math.min(width, height) / 2 - (margin / 2)

    // append the svg object to the div called 'my_dataviz'
    var svg = d3.select("#body13")
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    // set the color scale
    var color = d3.scaleOrdinal()
      .domain(data)
      .range(["#FF9AA2", "#FFB7B2", "#FFDAC1", "#E2F0CB", "#B5EAD7", "#C7CEEA"]);

    // Compute the position of each group on the pie:
    var pie = d3.pie()
      .value(function (d) { return d.value; })
    var data_ready = pie(d3.entries(data))
    // Now I know that group A goes from 0 degrees to x degrees and so on.

    // shape helper to build arcs:
    var arcGenerator = d3.arc()
      .innerRadius(0)
      .outerRadius(radius)

    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    svg
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arcGenerator)
      .attr('fill', function (d) { return (color(d.data.key)) })
      .attr("stroke", "black")
      .style("stroke-width", "2px")
      .append("title")
      .text(d => `Average Preventable Deaths: ${d.data.value.toLocaleString()}`);

    // Now add the annotation. Use the centroid method to get the best coordinates
    svg
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('text')
      .text(function (d) { return d.data.key })
      .attr("transform", function (d) { return "translate(" + arcGenerator.centroid(d) + ")"; })
      .style("text-anchor", "middle")
      .style("font-size", 17)
  }

  render() {
    return (
      <div style={{ justifyContent: "center" }}>
        <h1 style={{ textAlign: "center" }}> Average Preventable Deaths Per Region</h1>
        <p></p>
        <div style={{ justifyContent: "center" }}>
          <svg id='body13' width="100%" height={650}></svg>
        </div>

      </div>
    );
  }
}