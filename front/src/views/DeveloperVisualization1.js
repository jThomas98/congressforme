import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';

export default class DeveloperVisualization1 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      regions: [],
      loaded: false
    };

    this.createBarGraph = this.createBarGraph.bind(this);
    this.fetchLocations = this.fetchLocations.bind(this);
  }

  componentDidMount() {
    this.fetchLocations();
    if (this.state.loaded) {
      this.createBarGraph();
    }
  }

  componentDidUpdate() {
    if (this.state.loaded) {
      this.createBarGraph();
    }
  }


  fetchLocations() {
    const headers = { 'Access-Control-Allow-Origin': '*' };

    const api = 'https://api.thehealthodyssey.com/api/locations';
    const my_map = new Map();
    axios.get(api, headers).then((response) => {
      const data = response.data;
      const num_results = data.num_results;
      const objects = data.objects;
      for (let objectsIdx = 0; objectsIdx < num_results; ++objectsIdx) {
        const object = objects[objectsIdx];
        const region = object.region;
        if (my_map.has(region)) {

          // update the total with the mortality rate of this state and increment the count
          const average_mortality_rate = my_map.get(region);
          average_mortality_rate.total += object.mortality_rate;
          average_mortality_rate.count += 1;
          my_map.set(region, average_mortality_rate);
        } else {

          // create an object that keeps track of the mortality rate and the count of the number of states per region
          const average_mortality_rate = {
            total: object.mortality_rate,
            count: 1
          };
          console.log(my_map);
          my_map.set(region, average_mortality_rate);
        }
      }

      // compute the averages and push them into a new regions array
      const my_regions = [];
      for (const [key, value] of my_map.entries()) {
        const my_name = key;
        const average = value.total / value.count;
        const region = {
          name: my_name,
          mortality_rate: average
        };
        my_regions.push(region);
      };
      this.setState({ regions: my_regions, loaded: true });
    });
  }

  createBarGraph() {
    let margin = ({ top: 20, right: 0, bottom: 30, left: 40 });
    let width = window.screen.width - 600;
    let height = 650;

    const locations = this.state.regions;

    let x = d3.scaleBand()
      .domain(locations.map(d => d.name))
      .range([margin.left, width - margin.right])
      .padding(0.1);

    let maxCount = d3.max(locations, d => d.mortality_rate);
    let y = d3.scaleLinear()
      .domain([maxCount, 1]).nice()
      .range([margin.top, height - margin.bottom]);

    let xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x)
        .tickSizeOuter(0));

    let yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y))
      .call(g => g.select(".domain").remove());


    const svg = d3.select("#body12");

    svg.append("g")
      .attr("fill", "rgb(100, 266, 122)")
      .selectAll("rect").data(locations).enter().append("rect")
      .attr("x", d => x(d.name))
      .attr("y", d => y(d.mortality_rate))
      .attr("height", d => y(0) - y(d.mortality_rate))
      .attr("width", x.bandwidth());

    svg.append("g")
      .call(xAxis);

    svg.append("g")
      .call(yAxis);


  }

  render() {
    return (
      <div>
        <h1 style={{ textAlign: "center" }}> Average Mortality Rate per Region </h1>
        <div>
          <svg id='body12' width="100%" height={650}></svg>
        </div>

      </div>
    );
  }
}