import React from 'react';
import { Image } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';

class DistrictModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      district: null,
      districtLoaded: false,
      rep: null
    };

    this.loadCurrentDistrict = this.loadCurrentDistrict.bind(this);
    this.renderDistrict = this.renderDistrict.bind(this);
    this.renderLoadingPage = this.renderLoadingPage.bind(this);
  }

  componentDidMount() {
    this.loadCurrentDistrict();
  }

  loadCurrentDistrict() {
    const headers = { 'Access-Control-Allow-Origin': '*' };
    const myURL = window.location.href;

    let idStr;
    for (let i = myURL.length - 1; i >= 0; i--) {
      if (myURL.charAt(i) === "/") {
        idStr = myURL.substring(i + 1);
        break;
      }
    }

    console.log(idStr)

    const api = 'https://www.congressfor.me/api/districts/' + idStr;
    console.log(api)


    axios.get(api, headers).then((response) => {
      const myDistrict = (response.data);
      this.setState({ district: myDistrict, districtLoaded: true });
      console.log("axios district request done");
      this.loadRep()
    });
  }

  loadRep() {
    var self = this;
    const headers = { 'Access-Control-Allow-Origin': '*' };
    const api = 'https://www.congressfor.me/api/congressmen/'
      + self.state.district.congressid;
    axios.get(api, headers).then((response) => {
      const myRep = (response.data);
      const name = myRep.firstname + " " + myRep.lastname;
      const id = myRep.congressid;
      const linkString = "/Representatives/" + id;
      const reactElem = <Link to={linkString}>{name}</Link>
      this.setState({ rep: reactElem });
      console.log("axios representative request done");
    });
  }

  renderDistrict() {
    return (
      <div>
        <section className="section">
          <div className="container">
            <header className="section-header">
              <div className="text-center">
                <h1>{this.state.district.name}</h1>
                <Image src={"https://nationalmap.gov/small_scale/printable/images/preview/congdist/pagecgd113_"+this.state.district.state.toLowerCase()+".gif"}
                  height='495px' width='594px' />
                <hr />
              </div>
            </header>
          </div>
          <div>
            <Container>
              <Row className="justify-content-center">
                <Col md="auto">
                  <Card>
                    <Card.Header>Attributes:</Card.Header>
                    <ListGroup variant="flush">
                      <ListGroup.Item>State: {this.state.district.state}</ListGroup.Item>
                      <ListGroup.Item>Representative: {this.state.rep}</ListGroup.Item>
                      <ListGroup.Item>Party: {this.state.district.party}</ListGroup.Item>
                      <ListGroup.Item>District Number: District {this.state.district.number}</ListGroup.Item>
                      <ListGroup.Item>Election date: {this.state.district.election_date}</ListGroup.Item>
                    </ListGroup>
                  </Card>
                </Col>
                <Col>
                  <Card className="text-center">
                    <Card.Body>
                      <Card.Title>Description</Card.Title>
                      <Card.Text>{this.state.district.wiki}</Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </div>
    );
  }

  renderLoadingPage() {
    console.log("Render loading page called");
    return (
      <div>
        <section className="section">
          <div className="container">
            <header className="section-header">
              <div className="text-center">
                <h1>Loading district...</h1>
                <hr />
              </div>
            </header>
          </div>
        </section>
      </div>
    );
  }

  render() {
    console.log(this.state.districtLoaded);
    if (this.state.districtLoaded) {
      console.log("District loaded");
      return this.renderDistrict();
    } else {
      console.log("District did not load");
      return this.renderLoadingPage();
    }
  }
}

export default DistrictModel;