import React from 'react';
import { Image } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Container, Row, Col } from 'react-bootstrap';
import { Pie } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import DefaultPic from '../assets/DefaultCongressman.png';
import axios from 'axios';


export default class DonorModel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      donorid: this.props.match.donorid,
      donor: {},
      donorLoaded: false
    };


    this.renderDonorModel = this.renderDonorModel.bind(this);
    this.renderLoadingPage = this.renderLoadingPage.bind(this);
    this.loadDonorData = this.loadDonorData.bind(this);
  }

  componentDidMount() {
    this.loadDonorData();
  }

  loadDonorData() {
    const headers = { 'Access-Control-Allow-Origin': '*' };
    const myURL = window.location.href;

    let idStr;
    for (let i = myURL.length - 1; i >= 0; i--) {
      if (myURL.charAt(i) === "/") {
        idStr = myURL.substring(i + 1);
        break;
      }
    }

    console.log(idStr)

    const api = 'https://www.congressfor.me/api/donors/' + idStr;
    console.log(api)


    axios.get(api, headers).then((response) => {
      const myDonor = (response.data);
      if (myDonor.pic === "none") {
        myDonor.pic = DefaultPic;
      }
      this.setState({ donor: myDonor, donorLoaded: true });
      console.log("axios donor request done");
    });
  }


  renderDonorModel() {
    console.log(this.state.donor)
    return (
      <section className="section">
        <div className="container">
          <header className="section-header">
            <div className="text-center">
              <h1>{this.state.donor.name}</h1>
              <Image src={this.state.donor.pic}
                height='400px' width='400px' />
              <hr />
            </div>
          </header>
        </div>
        <div className="container">
          <Container>
            <Row className="justify-content-start">
              <Col lg="4">
                <Card>
                  <Card.Header>Attributes:</Card.Header>
                  <ListGroup variant="flush">
                    <ListGroup.Item>Democratic Contribution: {this.state.donor.dem_contributions}</ListGroup.Item>
                    <ListGroup.Item>Republican Contribution: {this.state.donor.rep_contributions}</ListGroup.Item>
                    <ListGroup.Item>Cycle: {this.state.donor.cycle}</ListGroup.Item>
                  </ListGroup>
                </Card>
                <hr />

              </Col>
              <Col lg="8">
                <Card>
                  <MDBContainer>
                    <h3 className="mt-5">Expenditures</h3>
                    <Pie data={{
                      labels: ["Democrat Contributions", "Republican Contributions"],
                      datasets: [
                        {
                          data: [this.state.donor.dem_contributions, this.state.donor.rep_contributions],
                          backgroundColor: [
                            "#0066ff",
                            "#F7464A"
                          ],
                          hoverBackgroundColor: [
                            "#3385ff",
                            "#FF5A5E"
                          ]
                        }
                      ]
                    }}
                      options={{ responsive: true }} />
                  </MDBContainer>
                </Card>
              </Col>

            </Row>
          </Container>
        </div>
      </section>
    );
  }

  renderLoadingPage() {
    console.log("Render loading page called");
    return (
      <div>
        <section className="section">
          <div className="container">
            <header className="section-header">
              <div className="text-center">
                <h1>Loading district...</h1>
                <hr />
              </div>
            </header>
          </div>
        </section>
      </div>
    );
  }
  render() {
    document.body.style.backgroundImage = "none";

    console.log(this.state.donorLoaded);
    if (this.state.donorLoaded) {
      console.log("Donor loaded");
      return this.renderDonorModel();
    } else {
      console.log("Donor did not load");
      return this.renderLoadingPage();
    }
  }
}