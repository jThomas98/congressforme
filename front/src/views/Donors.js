import React from 'react';
import { Table, Container, Row, Col, FormControl, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import BootstrapTable from 'react-bootstrap-table-next';
import Slider from '@material-ui/core/Slider'
import { getDonorsSearchQuery } from '../components/SearchQueries';
import { renderDonorSearchResultCard } from '../components/SearchResultCards';

class Donors extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchStr: "",
            searchTerms: [],
            currentPage: 0,
            instancesPerPage: 0,
            numPages: 0,
            donors: [],
            queryStr: "",
            demValue: [0, 3700000],
            repValue: [0, 3700000],
            demStatement: "",
            repStatement: "",
            filterStr: ""
        };

        this.handleSearchInput = this.handleSearchInput.bind(this);
        this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
        this.handleSearchResetSubmit = this.handleSearchResetSubmit.bind(this);
        this.renderCard = this.renderCard.bind(this);
        this.loadCurrentDonors = this.loadCurrentDonors.bind(this);
        this.valuetext = this.valuetext.bind(this);
        this.handleDemChange = this.handleDemChange.bind(this);
        this.handleRepChange = this.handleRepChange.bind(this);
    }

    componentDidMount() {
        this.loadCurrentDonors(this.state.currentPage, this.state.queryStr, this.state.filterStr, this.state.searchStr);
    }

    handleSearchInput(event) {
        this.setState({
            searchStr: event.target.value
        })
    }

    handleSearchSubmit(event) {
        event.preventDefault()
        this.setState({
            searchMode: true
        })
        this.loadCurrentDonors(0, this.state.queryStr, this.state.filterStr, this.state.searchStr)
    }

    handleSearchResetSubmit(event) {
        event.preventDefault()
        this.setState({
            searchStr: "",
            searchMode: false
        })
        this.loadCurrentDonors(0, this.state.queryStr, this.state.filterStr, "")
    }

    async loadCurrentDonors(currPage, queryString, filterString, searchString) {
        var self = this;
        let page = currPage + 1;
        const headers = { 'Access-Control-Allow-Origin': '*' };
        let currentDonors = []
        let searchTerms = searchString.split(" ")
        let api = ""

        if (searchString === "") {
            api = "https://www.congressfor.me/api/donors?page=" + page + "&q={\"order_by\":[" + queryString + "],\"filters\":[{\"and\":[" + filterString + "]}]}";
        } else {
            api = getDonorsSearchQuery(searchTerms, currPage)

        }

        await axios.get(api, headers).then(function (response) {
            console.log("in axios api:" + api);
            const objects = (response.data).objects;
            for (let i = 0; i < objects.length; ++i) {
                const donor = objects[i];
                const linkString = "/Donors/" + donor.donorid;
                donor.nametext = donor.name
                donor.name = <Link to={{ pathname: linkString }}>{donor.name}</Link>
                currentDonors.push(donor);
            }
            self.setState({
                searchStr: searchString,
                searchTerms: searchTerms,
                currentPage: currPage,
                instancesPerPage: currentDonors.length,
                numPages: response.data.total_pages,
                donors: currentDonors,
                queryStr: queryString,
                filterStr: filterString
            });
        });
    }

    renderCard(rep, index) {
        const searchTerms = this.state.searchTerms
        return renderDonorSearchResultCard(rep, index, searchTerms)
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.loadCurrentDonors(selected, this.state.queryStr, this.state.filterStr, this.state.searchStr);
    };

    handleDemChange = (event, newValue) => {
        this.setState({ demValue: newValue })
        let filter = "{\"name\":\"dem_contributions\",\"op\":\"gt\",\"val\":\"" + newValue[0] + "\"}, {\"name\":\"dem_contributions\",\"op\":\"lt\",\"val\":\"" + newValue[1] + "\"}";
        this.state.demStatement = filter;
        this.loadCurrentDonors(0, this.state.queryStr, filter, this.state.searchStr)
    };

    handleRepChange = (event, newValue) => {
        this.setState({ repValue: newValue })
        let filter = "{\"name\":\"rep_contributions\",\"op\":\"gt\",\"val\":\"" + newValue[0] + "\"}, {\"name\":\"rep_contributions\",\"op\":\"lt\",\"val\":\"" + newValue[1] + "\"}";
        this.state.repStatement = filter;
        this.loadCurrentDonors(0, this.state.queryStr, filter, this.state.searchStr)
    };

    buildFilterString() {
        let filter = "";
        if (this.state.demStatement === "" && this.state.repStatement !== "") {
            filter = this.state.repStatement;
        }
        else if (this.state.demStatement !== "" && this.state.repStatement === "") {
            filter = this.state.demStatement;
        }
        else {
            filter = this.state.demStatement + ", " + this.state.repStatement;
        }
        return filter;
    }


    valuetext(value) {
        return `${value}°C`;
    }

    render() {
        document.body.style.backgroundImage = "none";
        const columns = [{
            dataField: 'donorid',
            text: 'ID',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentDonors(0, "{\"field\":\"" + field + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }
        }, {
            dataField: 'name',
            text: 'Name',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentDonors(0, "{\"field\":\"" + field + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }

        }, {
            dataField: 'dem_contributions',
            text: 'Democratic Contributions',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentDonors(0, "{\"field\":\"" + field + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }
        }, {
            dataField: 'rep_contributions',
            text: 'Republican Contributions',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentDonors(0, "{\"field\":\"" + field + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }

        }, {
            dataField: 'cycle',
            text: 'Cycle',
        }
        ];
        return (
            <div>
                <Container className={"contentContainer"}>
                    <div className={"table"}>
                        <h1 style={{ textAlign: "center", margin: "25px" }}>Donors</h1>
                        <Container>
                            <Row className="justify-content-center">
                                <Col lg="4">
                                    <Row>
                                        <Form onSubmit={this.handleSearchSubmit} inline>
                                            <FormControl
                                                onChange={this.handleSearchInput}
                                                value={this.state.searchStr}
                                                type="text"
                                                placeholder="Search"
                                                className="mr-sm-2" />

                                            <Button type="submit"
                                                className="btn btn-info">
                                                Search
                                                </Button>
                                        </Form>
                                        {this.state.searchMode ?
                                            (<Button onClick={this.handleSearchResetSubmit} type="submit"
                                                className="btn btn-warning"
                                                style={{ marginLeft: "5px" }}>
                                                Reset
                                            </Button>)
                                            :
                                            (<div></div>)}
                                    </Row>
                                </Col>
                                {!this.state.searchMode ?
                                    (<Col lg="4">
                                        <Row>
                                            <h5 style={{ textAlign: "left", marginBottom: "10px" }}>Dem Contributions:</h5>
                                            <Slider
                                                max={5719398}
                                                style={{ width: '300px' }}
                                                value={this.state.demValue}
                                                onChange={this.handleDemChange}
                                                aria-labelledby="continuous-slider"
                                            />
                                        </Row>
                                        <Row>
                                            <p style={{ textAlign: "left", marginBottom: "10px" }}>Min:{this.state.demValue[0]} Max:{this.state.demValue[1]}</p>
                                        </Row>
                                    </Col>)
                                    :
                                    (<Col lg="4"></Col>)
                                }
                                {!this.state.searchMode ?
                                    (<Col lg="4">
                                        <Row>
                                            <h5 style={{ textAlign: "left", marginBottom: "10px" }}>Rep Contributions:</h5>
                                            <Slider
                                                max={5719398}

                                                style={{ width: '300px' }}
                                                value={this.state.repValue}
                                                onChange={this.handleRepChange}
                                                aria-labelledby="range-slider"
                                                getAriaValueText={this.valuetext}
                                            />
                                        </Row>
                                        <p style={{ textAlign: "left", marginBottom: "10px" }}>Min:{this.state.repValue[0]} Max:{this.state.repValue[1]}</p>
                                    </Col>)
                                    :
                                    (<Col lg="4"></Col>)
                                }
                            </Row>
                        </Container>
                        {this.state.searchMode ?
                            (<hr style={{ visibility: "hidden", marginTop: "4px", marginBottom: "4px" }} />)
                            :
                            (<div></div>)
                        }
                        {this.state.donors.length === 0 ?
                            <h2>Loading Donors...</h2>
                            :
                            (<div>
                                {this.state.searchMode ?
                                    (<div>
                                        <Table hover>
                                            <tbody>
                                                {this.state.donors.map(this.renderCard)}
                                            </tbody>
                                        </Table>
                                        <Container
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <Row>
                                                <ReactPaginate
                                                    previousLabel={'previous'}
                                                    nextLabel={'next'}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}
                                                    pageCount={this.state.numPages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={3}
                                                    onPageChange={this.handlePageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </Row>
                                        </Container>
                                    </div>)
                                    :
                                    (<div>
                                        <BootstrapTable keyField='donorid' data={this.state.donors} columns={columns} />

                                        <Container
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <Row>
                                                <ReactPaginate
                                                    previousLabel={'previous'}
                                                    nextLabel={'next'}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}
                                                    pageCount={this.state.numPages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={3}
                                                    onPageChange={this.handlePageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </Row>
                                        </Container>
                                    </div>)
                                }
                            </div>)
                        }
                    </div>
                </Container>
            </div>
        );
    }
}

export default Donors;
