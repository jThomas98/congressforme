import React, { Component } from "react";
import Carousel from 'react-bootstrap/Carousel'
import Welcomeslide from "../assets/background_home.jpg";
import Repslide from "../assets/repSlide.jpg";
import Disslide from "../assets/disSlide.jpg";
import iseslide from "../assets/iseSlide.jpg";
import Button from 'react-bootstrap/Button'
import { Image } from 'react-bootstrap';
import "./Home.css";

export default class Home extends Component {

  render() {
    return (
      // <div className="Home">
      <div>
        {/* dynamic front page carousel */}
        <Carousel id="carousel" style={{ width: 'auto', height: '100vh' }} >
          {/* Welcome slide */}
          <Carousel.Item >
            <Image
              id="slide-img"
              src={Welcomeslide}
              alt="First Slide"
              fluid
            />
            <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
              <h1>Congress For Me</h1>
              <p class="lead">Find out about your representatives, their districts, and how they receive funding</p>
              <Button variant="warning" href="/About" size='lg'>About Us</Button>
            </Carousel.Caption>
          </Carousel.Item>
           {/* Representatives slide */}
          <Carousel.Item>
            <Image
              id="slide-img"
              src={Repslide}
              alt="Second Slide"
              fluid
            />
            <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
              <h2>Meet the people who represent you</h2>
              <Button variant="warning" href="/Representatives" size='lg'>Find Representatives</Button>
            </Carousel.Caption>

          </Carousel.Item>
           {/* District slide */}
          <Carousel.Item>
            <img
              id="slide-img"
              src={Disslide}
              alt="Third Slide"
            />
            <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
              <h2>Learn more about your congressional district</h2>
              <Button variant="warning" href="/Districts" size='lg'>Find Districts</Button>
            </Carousel.Caption>
          </Carousel.Item>
           {/* Donor slide */}
          <Carousel.Item>
            <img
              id="slide-img"
              src={iseslide}
              alt="Fourth Slide"
            />
            <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
              <h2>Know where your representative's money is coming from</h2>
              <Button variant="warning" href="/Donors" size='lg'>Find Donors</Button>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
    );
  }
}

