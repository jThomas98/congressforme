import React, { Component } from 'react'
import { NavDropdown, Nav, Button, Form, FormControl, Navbar, NavItem } from 'react-bootstrap'
// import Nav from '@bit/react-bootstrap.react-bootstrap.nav'
// import Button from '@bit/react-bootstrap.react-bootstrap.button'
// import Form from '@bit/react-bootstrap.react-bootstrap.form'
// import FormControl from '@bit/react-bootstrap.react-bootstrap.form-control'
// import Navbar from '@bit/react-bootstrap.react-bootstrap.navbar'
// import ReactBootstrapStyle from '@bit/react-bootstrap.react-bootstrap.internal.style-links';

export default class Navigation extends Component {
    render() {
        return (
            <Navbar bg="light" expand="lg" style={{ width: 400 }}>
                <Navbar.Brand>Home</Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav>
                        <NavItem>ITEM1</NavItem>
                        <NavItem>ITEM2</NavItem>
                        <NavItem>ITEM3</NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

