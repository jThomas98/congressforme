import React, { Component } from 'react';
import { Image } from 'react-bootstrap';
import Img from '../assets/monkey.jpeg';
import "./NotFound.css";

export default class NotFound extends Component {
    render() {
        document.body.style.backgroundImage = "none";
        return (
            <div className="NotFound">
                <h3>Sorry, we can not seem to find that page.</h3>
                <Image src={Img} height='400px' width='800px' />
            </div>
        );
    }
}