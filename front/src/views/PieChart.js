import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';

export default class PieChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      partyMoney: [],
      loaded: false
    };

    this.createBubbleChart = this.createBubbleChart.bind(this);
    this.fetchPartyMoney = this.fetchPartyMoney.bind(this);
  }

  componentDidMount() {
    this.fetchPartyMoney();
    if (this.state.loaded) {
      console.log("finished loading... component did mount");
      this.createBubbleChart();
    }
  }

  componentDidUpdate() {
    if (this.state.loaded) {
      console.log("finshed loading... component did update");
      this.createBubbleChart();
    }
  }


  fetchPartyMoney() {
    const headers = { 'Access-Control-Allow-Origin': '*' };

    const api = 'https://www.congressfor.me/api/party_money';

    // get all of the representatives for the state, push them to a promises array 
    const promises = [];

    // numPages is known from looking at the data, hardcoded in
    let numPages = 5;
    for (let curPage = 1; curPage <= numPages; ++curPage) {
      const pagedApi = api + "?page=" + curPage;
      promises.push(axios.get(pagedApi, headers));
    }

    // myPartyMoney stores the states and their money amounts as objects, and is set to partyMoney 
    // at the end of the Promise
    const myPartyMoney = []

    Promise.all(promises).then((results) => {
      // the results of all of the requests are stored in the results variable, which is an array
      for (let resultIdx = 0; resultIdx < results.length; ++resultIdx) {
        const data = results[resultIdx].data;
        const objects = data.objects;
        // go through each result and actually store its data
        for (let dataResultIdx = 0; dataResultIdx < objects.length; ++dataResultIdx) {
          const object = objects[dataResultIdx];
          myPartyMoney.push(object.amount);
        }
      }
      this.setState({ partyMoney: myPartyMoney, loaded: true });
    });

  }

  createBubbleChart() {
    const data = { "Democrat": this.state.partyMoney[0], "Independent": this.state.partyMoney[1], "Republican": this.state.partyMoney[2] };

    var width = window.screen.width * .75
    var height = 450
    var margin = 40

    // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
    var radius = Math.min(width, height) / 2 - margin

    // append the svg object to the div called 'my_dataviz'
    var svg = d3.select("#body10")
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    // set the color scale
    var color = d3.scaleOrdinal()
      .domain(data)
      .range(["#3385ff", "#8a89a6", "#FF5A5E"]);

    // Compute the position of each group on the pie:
    var pie = d3.pie()
      .value(function (d) { return d.value; })
    var data_ready = pie(d3.entries(data))
    // Now I know that group A goes from 0 degrees to x degrees and so on.

    // shape helper to build arcs:
    var arcGenerator = d3.arc()
      .innerRadius(0)
      .outerRadius(radius)

    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    svg
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arcGenerator)
      .attr('fill', function (d) { return (color(d.data.key)) })
      .attr("stroke", "black")
      .style("stroke-width", "2px")
      .append("title")
      .text(d => `Total Donations: ${d.data.value.toLocaleString()}`);

    // Now add the annotation. Use the centroid method to get the best coordinates
    svg
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('text')
      .text(function (d) { return d.data.key })
      .attr("transform", function (d) { return "translate(" + arcGenerator.centroid(d) + ")"; })
      .style("text-anchor", "middle")
      .style("font-size", 17)
  }

  render() {
    return (
      <div style={{ justifyContent: "center" }}>
        <h1 style={{ textAlign: "center" }}> Total Money Donated by Party</h1>
        <p></p>
        <div style={{ justifyContent: "center" }}>
          <svg id='body10' width="100%" height={650}></svg>
        </div>

      </div>
    );
  }
}