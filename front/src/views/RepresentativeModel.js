import React from 'react';
import { Image } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Container, Row, Col } from 'react-bootstrap';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import { Link } from 'react-router-dom';
import DefaultPic from '../assets/DefaultCongressman.png';
import getCommittees from '../Committees';
import axios from 'axios';
import FacebookLogo from '../assets/facebook-logo.png'



class RepresentativeModel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            congressid: this.props.match.congressid,
            congressman: {},
            committees: "",
            donors: [],
            startLoadingDonors: false,
            donorsLoaded: false,
            congressManLoaded: false
        };

        this.renderContributors = this.renderContributors.bind(this);
        this.renderCongressman = this.renderCongressman.bind(this);
        this.renderLoadingPage = this.renderLoadingPage.bind(this);
        this.loadDonors = this.loadDonors.bind(this);
    }

    componentDidMount() {
        console.log("component did mount called");
        console.log(this.state.congressManLoaded);
        if (this.state.congressManLoaded) {
            console.log("I am in the if clause");
            // go through the contributors
            const donorIDs = [];

            // get the donors object from the congressman
            const congressDonors = this.state.congressman.congressdonor;
            const lengthOfDonors = Math.min(congressDonors.length, 5);
            for (let i = 0; i < lengthOfDonors; ++i) {
                const donor = congressDonors[i];
                donorIDs.push(donor.donorid);
            }

            const headers = { 'Access-Control-Allow-Origin': '*' };
            // we have all of the donor ID's, now we make a bunch of get requests to get the donor names
            const promises = [];
            for (let i = 0; i < donorIDs.length; ++i) {
                const api = 'https://www.congressfor.me/api/donors/' + donorIDs[i];
                promises.push(axios.get(api, headers));
            }

            const donorReactElems = [];

            Promise.all(promises).then((results) => {
                for (let i = 0; i < results.length; i++) {
                    const data = results[i].data;
                    // console.log(data);
                    const name = data.name;
                    const id = data.donorid;
                    const linkString = "/Donors/" + id;
                    const reactElem = <ListGroup.Item key={i}><Link to={linkString}>{name}</Link></ListGroup.Item>
                    donorReactElems.push(reactElem);
                }
                if (donorReactElems.length === 0) {
                    donorReactElems.push(<ListGroup.Item key={0}>No donors found.</ListGroup.Item>);
                }
                this.setState({ donors: donorReactElems, donorsLoaded: true });
            });
        } else {
            // the congressid must be passed in otherwise this will not work
            const headers = { 'Access-Control-Allow-Origin': '*' };
            const myURL = window.location.href;
            const num3 = myURL[myURL.length - 1];
            const num2 = myURL[myURL.length - 2];
            const num1 = myURL[myURL.length - 3];

            let idStr;
            if (num1 === '/') {
                idStr = num2 + num3;
            } else {
                idStr = num1 + num2 + num3;
            }
            const api = 'https://www.congressfor.me/api/congressmen/' + idStr;
            console.log("I am in the else clause ");
            axios.get(api, headers).then((response) => {

                const myCongressman = (response.data);
                if (myCongressman.pic === "none") {
                    myCongressman.pic = DefaultPic;
                }
                const myCommittees = getCommittees(myCongressman.congresscommittee);
                console.log(myCongressman);
                this.setState({ congressman: myCongressman, committees: myCommittees, congressManLoaded: true, startLoadingDonors: true });
                console.log("axios request done");
                this.loadDonors();
            });
        }
    }

    loadDonors() {
        console.log("I am in the if clause");
        // go through the contributors
        const donorIDs = [];

        // get the donors object from the congressman
        const congressDonors = this.state.congressman.congressdonor;
        const lengthOfDonors = Math.min(congressDonors.length, 5);
        for (let i = 0; i < lengthOfDonors; ++i) {
            const donor = congressDonors[i];
            donorIDs.push(donor.donorid);
        }

        const headers = { 'Access-Control-Allow-Origin': '*' };
        // we have all of the donor ID's, now we make a bunch of get requests to get the donor names
        const promises = [];
        for (let i = 0; i < donorIDs.length; ++i) {
            const api = 'https://www.congressfor.me/api/donors/' + donorIDs[i];
            promises.push(axios.get(api, headers));
        }

        const donorReactElems = [];

        Promise.all(promises).then((results) => {
            for (let i = 0; i < results.length; i++) {
                const data = results[i].data;
                // console.log(data);
                const name = data.name;
                const id = data.donorid;
                const linkString = "/Donors/" + id;
                const reactElem = <ListGroup.Item key={i}><Link to={linkString}>{name}</Link></ListGroup.Item>
                donorReactElems.push(reactElem);
            }
            if (donorReactElems.length === 0) {
                donorReactElems.push(<ListGroup.Item key={0}>No donors found.</ListGroup.Item>);
            }
            this.setState({ donors: donorReactElems, donorsLoaded: true });
        });
    }

    renderCongressman() {
        let url = "https://www.facebook.com";
        if (this.state.congressman.facebook !== "None") {
            url += "/" + this.state.congressman.facebook;
        }
        console.log("render congressman called");
        console.log("render congressman: " + this.state.congressManLoaded);
        // under the assumption that this.state.congressman has been loaded in already
        return (
            <div>
                <section className="section">
                    <div className="container">
                        <header className="section-header">
                            <div className="text-center">
                                <h1>{this.state.congressman.firstname + " " + this.state.congressman.lastname}</h1>
                                <Image src={this.state.congressman.pic}
                                    height='400px' width='400px' />
                                <hr />
                            </div>
                        </header>
                    </div>
                    <div>
                        <Container>
                            <Row>
                                <Col>
                                    <Card className="justify-content-start">
                                        <Card.Header>Attributes:</Card.Header>
                                        <Card.Body>
                                            <ListGroup variant="flush">
                                                <ListGroup.Item>State: {this.state.congressman.state}</ListGroup.Item>
                                                <ListGroup.Item><Link to={"/Districts/" + this.state.congressman.districtid}>District: {this.state.congressman.district}</Link></ListGroup.Item>
                                                <ListGroup.Item>Committees: {this.state.committees}</ListGroup.Item>
                                                <ListGroup.Item>Party: {this.state.congressman.party}</ListGroup.Item>
                                                <ListGroup.Item>Session: {this.state.congressman.lastsession}</ListGroup.Item>
                                                <ListGroup.Item>Phone: {this.state.congressman.phone}</ListGroup.Item>
                                                <ListGroup.Item> <a href={this.state.congressman.url}> Visit Their Website</a> </ListGroup.Item>
                                            </ListGroup>
                                        </Card.Body>
                                    </Card>
                                </Col>
                                <Col>
                                    <Card>
                                        <Card.Header>Top Contributors</Card.Header>
                                        <ListGroup variant="flush">
                                            {!this.state.startLoadingDonors ? <ListGroup.Item>Loading Donors...</ListGroup.Item> : this.renderContributors()}
                                        </ListGroup>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <div className="container" style={{ paddingTop: "80px" }}>
                        <header className="section-header">
                            <div className="text-center">
                                <h1>Social Media</h1>
                                <hr />
                            </div>
                        </header>
                        <Container>
                            <Row className="justify-content-center">
                                <Col md="auto">
                                    {this.state.congressman.twitter ? <TwitterTimelineEmbed sourceType="profile" screenName={this.state.congressman.twitter} options={{ height: 400, width: 400 }} /> : <h2>No social media...</h2>}
                                </Col>
                                <Col md="auto">
                                    <a href={url}><img src={FacebookLogo} style={{ width: "180px" }} alt="Not found." ></img></a>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </section>
            </div>
        );
    }

    renderContributors() {
        console.log("Rendering contributors called");
        const donorReactElems = [];
        for (let i = 0; i < this.state.donors.length; i++) {
            const reactElem = this.state.donors[i];
            donorReactElems.push(reactElem);
        }
        return donorReactElems;
    }

    renderLoadingPage() {
        console.log("render loading page called");
        return (
            <div>
                <section className="section">
                    <div className="container">
                        <header className="section-header">
                            <div className="text-center">
                                <h1>Loading congressman...</h1>
                                <hr />
                            </div>
                        </header>
                    </div>
                </section>
            </div>
        );
    }

    render() {
        console.log(this.state.congressManLoaded);
        if (this.state.congressManLoaded) {
            console.log("Congressman loaded is true");
            return this.renderCongressman();
        } else {
            console.log("congress loaded is false");
            return this.renderLoadingPage();
        }
    }
}

export default RepresentativeModel;