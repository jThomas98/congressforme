import React from 'react';
import { Container, Row, Col, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import BootstrapTable from 'react-bootstrap-table-next';
import "../styles/Representatives.css"
import Select from "react-dropdown-select";
import { Table, Button, FormControl, Form } from "react-bootstrap";
import { getRepsSearchQuery } from '../components/SearchQueries';
import { renderRepSearchResultCard } from '../components/SearchResultCards';

// main logic:
/**
 * Get the Representatives from the API
 * Count the number of Representatives there are, store it in a variable
 * Divide the number of representatives by the number of people per page
 * Keep track of the current page
 * Fill the slots of the table with the information from the array for representative [currentPage - 1 * numberOfRepsPerPage] through [currentPage * numberOfRepsPerPage]
 * Get the different instances one time at the beginning
 * During every click just refill the array
 */

class Representatives extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchStr: "",
            searchTerms: [],
            searchMode: false,
            currentPage: 0,
            instancesPerPage: 0,
            numPages: 0,
            congressmen: [],
            queryStr: "",
            stateFilter: "",
            partyFilter: "",
            filterStr: "",
            buttonText: "None"
        };

        this.handleSearchInput = this.handleSearchInput.bind(this);
        this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
        this.handleSearchResetSubmit = this.handleSearchResetSubmit.bind(this);
        this.handleButtonChange = this.handleButtonChange.bind(this);
        this.handleStateDropdownChange = this.handleStateDropdownChange.bind(this);
        this.setButtonText = this.setButtonText.bind(this);
        this.buildFilterString = this.buildFilterString.bind(this);
        this.renderCard = this.renderCard.bind(this);
        this.loadCurrentCongressmen = this.loadCurrentCongressmen.bind(this);
        this.setButtonText = this.setButtonText.bind(this);
        this.handleButtonChange = this.handleButtonChange.bind(this);
        this.handleStateDropdownChange = this.handleStateDropdownChange.bind(this);
        this.buildFilterString = this.buildFilterString.bind(this);
    }

    componentDidMount() {
        this.loadCurrentCongressmen(this.state.currentPage, this.state.queryStr, this.state.filterStr, this.state.searchStr);
    }

    handleSearchInput(event) {
        this.setState({
            searchStr: event.target.value
        })
    }

    handleSearchSubmit(event) {
        event.preventDefault()
        this.setState({
            searchMode: true
        })
        this.loadCurrentCongressmen(0, this.state.queryStr, this.state.filterStr, this.state.searchStr)
    }

    handleSearchResetSubmit(event) {
        event.preventDefault()
        this.setState({
            searchStr: "",
            searchMode: false
        })
        this.loadCurrentCongressmen(0, this.state.queryStr, this.state.filterStr, "")
    }

    handleButtonChange(e) {
        const value = e.currentTarget.textContent;
        this.setButtonText(value);
    }

    handleStateDropdownChange(e) {
        console.log(e);
        let value = "";
        if (e[0]) {
            value = e[0].label;
        }
        this.setStateFilter(value);
    }

    setStateFilter(my_str) {
        let filter = ""
        if (my_str !== "") {
            filter = "{\"name\":\"state\",\"op\":\"eq\",\"val\":\"" + my_str + "\"}"
        }
        this.state.stateFilter = filter;
        this.loadCurrentCongressmen(0, this.state.queryStr, this.buildFilterString(), this.state.searchStr)
    }

    setButtonText(my_str) {
        let filter = ""
        if (my_str !== "None") {
            filter = "{\"name\":\"party\",\"op\":\"eq\",\"val\":\"" + my_str + "\"}"
        }
        this.state.partyFilter = filter;
        this.setState({ buttonText: my_str });
        this.loadCurrentCongressmen(0, this.state.queryStr, this.buildFilterString(), this.state.searchStr)
    }

    buildFilterString() {
        let filter = "";
        if (this.state.stateFilter === "" && this.state.partyFilter !== "") {
            filter = this.state.partyFilter;
        }
        else if (this.state.stateFilter !== "" && this.state.partyFilter === "") {
            filter = this.state.stateFilter;
        }
        else {
            filter = this.state.partyFilter + ", " + this.state.stateFilter;
        }
        return filter;
    }

    async loadCurrentCongressmen(currPage, queryString, filterString, searchString) {
        var self = this;
        let page = currPage + 1;
        const headers = { 'Access-Control-Allow-Origin': '*' };
        let currentCongressmen = []
        let searchTerms = searchString.split(" ");
        let api = ""
        api = (searchString === "") ? "".concat("https://www.congressfor.me/api/congressmen?page=", page, "&q={\"order_by\":[", queryString, "],", "\"filters\":[{\"and\":[", filterString, "]}]}")
                                    : getRepsSearchQuery(searchTerms, currPage);

        console.log("Reps Api query: " + api);

        await axios.get(api, headers).then(function (response) {
            const objects = (response.data).objects;
            for (let i = 0; i < objects.length; ++i) {
                const congressman = objects[i];
                const linkString = "/Representatives/" + congressman.congressid;
                congressman.fullname = <Link to={{ pathname: linkString }}>{congressman.firstname + " " + congressman.lastname}</Link>
                currentCongressmen.push(congressman);
            }
            self.setState({
                searchStr: searchString,
                searchTerms: searchTerms,
                currentPage: currPage,
                instancesPerPage: currentCongressmen.length,
                numPages: response.data.total_pages,
                congressmen: currentCongressmen,
                queryStr: queryString,
                filterStr: filterString
            });
        });
    }

    renderCard(rep, index) {
        const searchTerms = this.state.searchTerms
        return renderRepSearchResultCard(rep, index, searchTerms)
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.loadCurrentCongressmen(selected, this.state.queryStr, this.state.filterStr, this.state.searchStr);
    };

    render() {
        document.body.style.backgroundImage = "none";
        // store the representatives in the state or something
        // get the count
        // get the number of instances per page ( we can decide this )
        // Math.ceil(count / numInstances) to get the number of pages

        // then replace the trs wil the tableInfo and we're done
        const states = [
            { label: "AL", value: 1 },
            { label: "AK", value: 2 },
            { label: "AZ", value: 3 },
            { label: "AR", value: 4 },
            { label: "CA", value: 5 },
            { label: "CO", value: 6 },
            { label: "CT", value: 7 },
            { label: "DE", value: 8 },
            { label: "FL", value: 9 },
            { label: "GA", value: 10 },
            { label: "HI", value: 11 },
            { label: "ID", value: 12 },
            { label: "IL", value: 13 },
            { label: "IN", value: 14 },
            { label: "IA", value: 15 },
            { label: "KS", value: 16 },
            { label: "KY", value: 17 },
            { label: "LA", value: 18 },
            { label: "ME", value: 19 },
            { label: "MD", value: 20 },
            { label: "MA", value: 21 },
            { label: "MI", value: 22 },
            { label: "MN", value: 23 },
            { label: "MS", value: 24 },
            { label: "MO", value: 25 },
            { label: "MT", value: 26 },
            { label: "NE", value: 27 },
            { label: "NV", value: 28 },
            { label: "NH", value: 29 },
            { label: "NJ", value: 30 },
            { label: "NM", value: 31 },
            { label: "NY", value: 32 },
            { label: "NC", value: 33 },
            { label: "ND", value: 34 },
            { label: "OH", value: 35 },
            { label: "OK", value: 36 },
            { label: "OR", value: 37 },
            { label: "PA", value: 38 },
            { label: "RI", value: 39 },
            { label: "SC", value: 40 },
            { label: "SD", value: 41 },
            { label: "TN", value: 42 },
            { label: "TX", value: 43 },
            { label: "UT", value: 44 },
            { label: "VT", value: 45 },
            { label: "VA", value: 46 },
            { label: "WA", value: 47 },
            { label: "WV", value: 48 },
            { label: "WI", value: 49 },
            { label: "WY", value: 50 },
        ];
        const columns = [{
            dataField: 'fullname',
            text: 'Name',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentCongressmen(0, "{\"field\":\"" + "lastname" + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }
        }, {
            dataField: 'state',
            text: 'State',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentCongressmen(0, "{\"field\":\"" + field + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }

        }, {
            dataField: 'district',
            text: 'District'
        }, {
            dataField: 'party',
            text: 'Party',
            sort: true,

            onSort: (field, order) => {
                console.log("In on sort");
                this.loadCurrentCongressmen(0, "{\"field\":\"" + field + "\",\"direction\":\"" + order + "\"}", this.state.filterStr, this.state.searchStr);
            },
            sortFunc: (a, b, order, dataField, rowA, rowB) => {
                return 0;
            },
            sortCaret: (order, column) => {
                if (!order) return (<span>&nbsp;&nbsp;<font color="lightgray">▼ ▲</font></span>);
                else if (order === 'asc') return (<span>&nbsp;&nbsp;▼ <font color="lightgray">▲</font></span>);
                else if (order === 'desc') return (<span>&nbsp;&nbsp;<font color="lightgray">▼</font> ▲</span>);
                return null;
            }

        }, {
            dataField: 'lastsession',
            text: 'Session',
        }
        ];
        return (
            <div>
                <Container className={"contentContainer"}>
                    <div className={"table"}>
                        <h1 style={{ textAlign: "center", margin: "25px" }}>Representatives</h1>
                        <Container>
                            <Row className="justify-content-center">
                                <Col lg="6">
                                    <Row>
                                        <Form onSubmit={this.handleSearchSubmit} inline>
                                            <FormControl
                                                onChange={this.handleSearchInput}
                                                value={this.state.searchStr}
                                                type="text"
                                                placeholder="Search"
                                                className="mr-sm-2" />

                                            <Button type="submit"
                                                className="btn btn-info">
                                                Search
                                                </Button>
                                        </Form>
                                        {this.state.searchMode ?
                                            (<Button onClick={this.handleSearchResetSubmit} type="submit"
                                                className="btn btn-warning"
                                                style={{ marginLeft: "5px" }}>
                                                Reset
                                            </Button>)
                                            :
                                            (<div></div>)}
                                    </Row>
                                </Col>
                                {!this.state.searchMode ?
                                    (<Col lg="3" >
                                        <Row>
                                            <h5 style={{ marginBottom: "10px", marginRight: "10px" }}>Party:</h5>
                                            <Dropdown style={{ marginBottom: "10px", marginRight: "10px" }}>
                                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                                    {this.state.buttonText}
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu>
                                                    <Dropdown.Item href="#/action-1" onClick={this.handleButtonChange} >Democrat</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-2" onClick={this.handleButtonChange} >Republican</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-3" onClick={this.handleButtonChange} >None</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </Row>
                                    </Col>)
                                    :
                                    (<Col lg="3"></Col>)
                                }
                                {!this.state.searchMode ?
                                    (<Col lg="3">
                                        <Row>
                                            <h5 style={{ marginBottom: "10px", marginRight: "10px" }}>State:</h5>
                                            <Select clearable="true" options={states} values={[]} onChange={this.handleStateDropdownChange} />
                                        </Row>
                                    </Col>)
                                    :
                                    (<Col lg="3"></Col>)
                                }
                            </Row>
                        </Container>
                        {this.state.searchMode ?
                            (<hr style={{ visibility: "hidden", marginTop: "4px", marginBottom: "4px" }} />)
                            :
                            (<div></div>)
                        }
                        {this.state.congressmen.length === 0 ?
                            <h2>Loading congressmen...</h2>
                            :
                            (<div>
                                {this.state.searchMode ?
                                    (<div>
                                        <Table hover>
                                            <tbody>
                                                {this.state.congressmen.map(this.renderCard)}
                                            </tbody>
                                        </Table>
                                        <Container
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <Row>
                                                <ReactPaginate
                                                    previousLabel={'previous'}
                                                    nextLabel={'next'}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}
                                                    pageCount={this.state.numPages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={3}
                                                    onPageChange={this.handlePageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </Row>
                                        </Container>
                                    </div>)
                                    :
                                    <div>
                                        <BootstrapTable keyField='congressid' data={this.state.congressmen} columns={columns} />
                                        <Container
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <Row>
                                                <ReactPaginate
                                                    previousLabel={'previous'}
                                                    nextLabel={'next'}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}
                                                    pageCount={this.state.numPages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={3}
                                                    onPageChange={this.handlePageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </Row>
                                        </Container>
                                    </div>
                                }
                            </div>
                            )
                        }
                    </div>
                </Container>
            </div >
        );
    }
}

export default Representatives;
