import React, { Component } from "react";
import axios from 'axios';
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import { Table, Container, Row, Tabs, Tab } from 'react-bootstrap';

export default class Search extends Component {

    constructor(props) {
        super(props)

        this.state = {
            searchText: "",
            repsCurrPage: 0,
            repsInstancesPerPage: 0,
            repsNumPages: 0,
            reps: [],
            districtsCurrPage: 0,
            districtsInstancesPerPage: 0,
            districtsNumPages: 0,
            districts: [],
            districtReps: [],
            donorsCurrPage: 0,
            donorsInstancesPerPage: 0,
            donorsNumPages: 0,
            donors: [],
        }

        this.renderRepRow = this.renderRepRow.bind(this);
        this.renderDistrictRow = this.renderDistrictRow.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    async handleSearch(repsCurrPage, districtsCurrPage, donorsCurrPage) {
        var self = this;
        let searchText = this.props.location.state.searchText;
        const headers = { 'Access-Control-Allow-Origin': '*' };


        // Get congressmen
        let repsPage = repsCurrPage + 1;
        let currReps = []
        let repResponse;
        let repApi = "https://www.congressfor.me/api/congressmen?q={%22filters%22:%20[{%22name%22:%20%22fullname%22,%20%22op%22:%20%22ilike%22,%20%22val%22:%20%22%" + searchText + "%%22}]}&page=" + repsPage
        await axios.get(repApi, headers).then(function (response) {
            repResponse = response
            const objects = (repResponse.data).objects;
            for (let i = 0; i < objects.length; ++i) {
                const rep = objects[i];
                currReps.push(rep);
            }
            self.setState({
                searchText: searchText,
                repsCurrPage: repsCurrPage,
                repsInstancesPerPage: currReps.length,
                repsNumPages: repResponse.data.total_pages,
                reps: currReps
            })
        });

        // Get districts
        let districtsPage = districtsCurrPage + 1;
        let currDistricts = []
        let districtResponse;
        let districtApi = "https://www.congressfor.me/api/districts?q={%22filters%22:%20[{%22name%22:%20%22name%22,%20%22op%22:%20%22ilike%22,%20%22val%22:%20%22%" + searchText + "%%22}]}&page=" + districtsPage
        await axios.get(districtApi, headers).then(function (response) {
            districtResponse = response
            const objects = (districtResponse.data).objects;
            for (let i = 0; i < objects.length; ++i) {
                const district = objects[i];
                currDistricts.push(district);
            }
            self.setState({
                districtsCurrPage: districtsCurrPage,
                districtsInstancesPerPage: currDistricts.length,
                districtsNumPages: districtResponse.data.total_pages,
                districts: currDistricts
            })
            self.loadDistrictReps()
        })

        // Get donors
        let donorsPage = donorsCurrPage + 1;
        let currDonors = []
        let donorsResponse;
        let donorsApi = "https://www.congressfor.me/api/donors?q={%22filters%22:%20[{%22name%22:%20%22name%22,%20%22op%22:%20%22ilike%22,%20%22val%22:%20%22%" + searchText + "%%22}]}&page=" + donorsPage
        await axios.get(donorsApi, headers).then(function (response) {
            donorsResponse = response
            const objects = (donorsResponse.data).objects;
            for (let i = 0; i < objects.length; ++i) {
                const donor = objects[i];
                currDonors.push(donor);
            }
            self.setState({
                donorsCurrPage: donorsCurrPage,
                donorsInstancesPerPage: currDonors.length,
                donorsNumPages: donorsResponse.data.total_pages,
                donors: currDonors
            })
        })
    };

    loadDistrictReps() {
        var self = this;
        const headers = { 'Access-Control-Allow-Origin': '*' };
        const promises = [];
        for (let i = 0; i < self.state.districts.length; ++i) {
            const api = 'https://www.congressfor.me/api/congressmen/'
                + self.state.districts[i].congressid;
            promises.push(axios.get(api, headers));
        }
        const repReactElems = [];
        Promise.all(promises).then((results) => {
            for (let i = 0; i < results.length; i++) {
                const data = results[i].data;
                const name = data.firstname + " " + data.lastname;
                const id = data.congressid;
                const linkString = "/Representatives/" + id;
                const reactElem = <td key={i}><Link to={linkString}>{name}</Link></td>
                repReactElems.push(reactElem);
            }
            this.setState({ districtReps: repReactElems });
        });
    }

    renderRepRow(rep, index) {
        const r = rep;
        const linkString = "/Representatives/" + r.congressid;
        return (
            <tr key={index}>
                <td><Link to={{ pathname: linkString }}>{r.firstname + " " + r.lastname}</Link></td>
                <td>{r.state}</td>
                <td>{r.district}</td>
                <td>{r.party}</td>
                <td>{r.lastsession}</td>
            </tr>
        );
    }

    renderDistrictRow(district, index) {
        const d = district;
        const linkString = "/Districts/" + d.districtid;
        return (
            <tr key={index}>
                <td><Link to={{
                    pathname: linkString,
                    state: d
                }}>{d.name}</Link></td>
                <td>{d.state}</td>
                {this.state.districtReps[index]}
                <td>{d.party}</td>
                <td>District {d.number}</td>
                <td>{d.election_date}</td>
            </tr>
        );
    }

    renderDonorRow(donor, index) {
        const d = donor;
        const linkString = "/Donors/" + d.donorid;
        return (
            <tr key={index}>
                <td><Link to={{
                    pathname: linkString,
                    state: d
                }}>{d.donorid}</Link></td>
                <td>{d.name}</td>
                <td>${d.dem_contributions}</td>
                <td>${d.rep_contributions}</td>
                <td>{d.cycle}</td>
            </tr>
        );
    }

    handleRepsPageClick = data => {
        let selected = data.selected;
        this.handleSearch(selected, this.state.districtsCurrPage, this.state.donorsCurrPage);
    };

    handleDistrictsPageClick = data => {
        let selected = data.selected;
        this.handleSearch(this.state.repsCurrPage, selected, this.state.donorsCurrPage);
    };

    handleDonorsPageClick = data => {
        let selected = data.selected;
        this.handleSearch(this.state.repsCurrPage, this.state.districtsCurrPage, selected);
    };

    componentDidMount() {
        this.handleSearch(this.state.repsCurrPage, this.state.districtsCurrPage, this.state.donorsCurrPage);
    }

    componentDidUpdate(prevProps) {
        let prevSearch = prevProps.location.state.searchText;
        let newSearch = this.props.location.state.searchText;
        if (prevSearch !== newSearch) {
            this.handleSearch(0, 0, 0);
        }
    }

    render() {
        return (
            <div>

                <h1 style={{ textAlign: "center", margin: "25px" }}>Search Results for "{this.state.searchText}"</h1>
                <hr />
                <Container>
                    <Tabs defaultActiveKey="reps" id="uncontrolled-tab-example">
                        <Tab eventKey="reps" title="Representatives">
                            <hr style={{ visibility: "hidden" }} />
                            {this.state.reps.length == 0 ?
                                <h2>No Representatives Found.</h2> :
                                (
                                    <div>
                                        <Container>
                                            <Table striped bordered hover>
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>State</th>
                                                        <th>District</th>
                                                        <th>Party</th>
                                                        <th>Session</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.reps.map(this.renderRepRow)}
                                                </tbody>
                                            </Table>
                                        </Container>
                                        <Container
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <Row>
                                                <ReactPaginate
                                                    previousLabel={'previous'}
                                                    nextLabel={'next'}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}
                                                    pageCount={this.state.repsNumPages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={3}
                                                    onPageChange={this.handleRepsPageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </Row>
                                        </Container>
                                    </div>)
                            }
                        </Tab>
                        <Tab eventKey="districts" title="Districts">
                            <hr style={{ visibility: "hidden" }} />
                            {this.state.districts.length == 0 ?
                                <h2>No Districts Found.</h2> :
                                <div>
                                    <Container>
                                        <Table striped bordered hover>
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>State</th>
                                                    <th>Representative</th>
                                                    <th>Party</th>
                                                    <th>District</th>
                                                    <th>Election Dates:</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.districts.map(this.renderDistrictRow)}
                                            </tbody>
                                        </Table>
                                    </Container>
                                    <Container
                                        style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        <Row>
                                            <ReactPaginate
                                                previousLabel={'previous'}
                                                nextLabel={'next'}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.state.districtsNumPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={3}
                                                onPageChange={this.handleDistrictsPageClick}
                                                containerClassName={'pagination'}
                                                subContainerClassName={'pages pagination'}
                                                activeClassName={'active'}
                                            />
                                        </Row>
                                    </Container>
                                </div>
                            }
                        </Tab>
                        <Tab eventKey="donors" title="Donors">
                            <hr style={{ visibility: "hidden" }} />
                            {this.state.donors.length == 0 ?
                                <h2>No Donors Found.</h2> :
                                <div>
                                    <Container>
                                        <Table striped bordered hover>
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Democratic Contribution</th>
                                                    <th>Republican Contribution</th>
                                                    <th>Cycle</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.donors.map(this.renderDonorRow)}
                                            </tbody>
                                        </Table>
                                    </Container>
                                    <Container
                                        style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        <Row>
                                            <ReactPaginate
                                                previousLabel={'previous'}
                                                nextLabel={'next'}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.state.donorsNumPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={3}
                                                onPageChange={this.handleDonorsPageClick}
                                                containerClassName={'pagination'}
                                                subContainerClassName={'pages pagination'}
                                                activeClassName={'active'}
                                            />
                                        </Row>
                                    </Container>
                                </div>
                            }
                        </Tab>
                    </Tabs>
                </Container>
            </div>
        );
    }
}
