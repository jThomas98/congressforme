import React, { Component } from "react";
import ReactPaginate from 'react-paginate';
import { Table, Container, Row, Tabs, Tab } from 'react-bootstrap';
import { getRepsSearchQuery, getDistrictsSearchQuery, getDonorsSearchQuery } from '../components/SearchQueries';
import { getRepsSearchQueryResult, getDistrictsSearchQueryResult, getDonorsSearchQueryResult } from '../components/SearchQueryResults';
import { renderRepSearchResultCard, renderDistrictSearchResultCard, renderDonorSearchResultCard } from '../components/SearchResultCards';

export default class Search extends Component {

    constructor(props) {
        super(props)

        this.state = {
            searchText: "",
            searchTerms: [],
            repsCurrPage: 0,
            repsInstancesPerPage: 0,
            repsNumPages: 0,
            reps: [],
            districtsCurrPage: 0,
            districtsInstancesPerPage: 0,
            districtsNumPages: 0,
            districts: [],
            districtReps: [],
            donorsCurrPage: 0,
            donorsInstancesPerPage: 0,
            donorsNumPages: 0,
            donors: [],
        }

        this.renderRepCard = this.renderRepCard.bind(this);
        this.renderDistrictCard = this.renderDistrictCard.bind(this);
        this.renderDonorCard = this.renderDonorCard.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    async handleSearch(repsCurrPage, districtsCurrPage, donorsCurrPage) {
        var self = this;
        let searchText = (this.props.location.state !== undefined) ? this.props.location.state.searchText : "";
        let searchTerms = searchText.split(" ");

        console.log("Current search terms: " + searchTerms)

        let repApi = getRepsSearchQuery(searchTerms, repsCurrPage)
        console.log("Reps Search Query: " + repApi)
        let repSearchQueryResult = await getRepsSearchQueryResult(repApi)
        let repsNumPages = repSearchQueryResult.pages
        let currReps = repSearchQueryResult.reps

        let districtsApi = getDistrictsSearchQuery(searchTerms, districtsCurrPage)
        console.log("Districts Search Query: " + districtsApi)
        let districtsSearchQueryResult = await getDistrictsSearchQueryResult(districtsApi)
        let districtsNumPages = districtsSearchQueryResult.pages
        let currDistricts = districtsSearchQueryResult.districts

        let donorsApi = getDonorsSearchQuery(searchTerms, donorsCurrPage)
        console.log("Donors Search Query: " + donorsApi)
        let donorsSearchQueryResult = await getDonorsSearchQueryResult(donorsApi)
        let donorsNumPages = donorsSearchQueryResult.pages
        let currDonors = donorsSearchQueryResult.donors

        self.setState({
            searchText: searchText,
            searchTerms: searchTerms,
            repsCurrPage: repsCurrPage,
            repsInstancesPerPage: currReps.length,
            repsNumPages: repsNumPages,
            reps: currReps,
            districtsCurrPage: districtsCurrPage,
            districtsInstancesPerPage: currDistricts.length,
            districtsNumPages: districtsNumPages,
            districts: currDistricts,
            donorsCurrPage: donorsCurrPage,
            donorsInstancesPerPage: currDonors.length,
            donorsNumPages: donorsNumPages,
            donors: currDonors
        })
    };

    componentDidMount() {
        this.handleSearch(this.state.repsCurrPage, this.state.districtsCurrPage, this.state.donorsCurrPage);
    }

    componentDidUpdate(prevProps) {
        let prevSearch = (prevProps.location.state !== undefined) ? prevProps.location.state.searchText : "";
        let newSearch = (this.props.location.state !== undefined) ? this.props.location.state.searchText : "";
        if (prevSearch !== newSearch) {
            this.handleSearch(0, 0, 0);
        }
    }

    renderRepCard(rep, index) {
        const searchTerms = this.state.searchTerms
        return renderRepSearchResultCard(rep, index, searchTerms)
    }

    renderDistrictCard(district, index) {
        const searchTerms = this.state.searchTerms
        return renderDistrictSearchResultCard(district, index, searchTerms)
    }

    renderDonorCard(donor, index) {
        const searchTerms = this.state.searchTerms
        return renderDonorSearchResultCard(donor, index, searchTerms)
    }

    handleRepsPageClick(data) {
        let selected = data.selected;
        this.handleSearch(selected, this.state.districtsCurrPage, this.state.donorsCurrPage);
    };

    handleDistrictsPageClick = data => {
        let selected = data.selected;
        this.handleSearch(this.state.repsCurrPage, selected, this.state.donorsCurrPage);
    };

    handleDonorsPageClick = data => {
        let selected = data.selected;
        this.handleSearch(this.state.repsCurrPage, this.state.districtsCurrPage, selected);
    };

    render() {
        return (
            <div>
                <h1 style={{ textAlign: "center", margin: "25px" }}>Search Results for "{this.state.searchText}"</h1>
                <Container>
                    <Tabs defaultActiveKey="reps" id="uncontrolled-tab-example">
                        <Tab eventKey="reps" title="Representatives">
                            <hr style={{ visibility: "hidden" }} />
                            {this.state.reps.length === 0 ?
                                <h2>No Representatives Found.</h2>
                                :
                                (<div>
                                    <Container>
                                        <Table hover>
                                            <tbody>
                                                {this.state.reps.map(this.renderRepCard)}
                                            </tbody>
                                        </Table>
                                    </Container>
                                    <Container
                                        style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        <Row>
                                            <ReactPaginate
                                                previousLabel={'previous'}
                                                nextLabel={'next'}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.state.repsNumPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={3}
                                                onPageChange={this.handleRepsPageClick}
                                                containerClassName={'pagination'}
                                                subContainerClassName={'pages pagination'}
                                                activeClassName={'active'}
                                            />
                                        </Row>
                                    </Container>
                                </div>)
                            }
                        </Tab>
                        <Tab eventKey="districts" title="Districts">
                            <hr style={{ visibility: "hidden" }} />
                            {this.state.districts.length === 0 ?
                                <h2>No Districts Found.</h2>
                                :
                                (<div>
                                    <Container>
                                        <Table hover>
                                            <tbody>
                                                {this.state.districts.map(this.renderDistrictCard)}
                                            </tbody>
                                        </Table>
                                    </Container>
                                    <Container
                                        style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        <Row>
                                            <ReactPaginate
                                                previousLabel={'previous'}
                                                nextLabel={'next'}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.state.districtsNumPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={3}
                                                onPageChange={this.handleDistrictsPageClick}
                                                containerClassName={'pagination'}
                                                subContainerClassName={'pages pagination'}
                                                activeClassName={'active'}
                                            />
                                        </Row>
                                    </Container>
                                </div>)
                            }
                        </Tab>
                        <Tab eventKey="donors" title="Donors">
                            <hr style={{ visibility: "hidden" }} />
                            {this.state.donors.length === 0 ?
                                <h2>No Donors Found.</h2>
                                :
                                <div>
                                    <Container>
                                        <Table hover>
                                            <tbody>
                                                {this.state.donors.map(this.renderDonorCard)}
                                            </tbody>
                                        </Table>
                                    </Container>
                                    <Container
                                        style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        <Row>
                                            <ReactPaginate
                                                previousLabel={'previous'}
                                                nextLabel={'next'}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.state.donorsNumPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={3}
                                                onPageChange={this.handleDonorsPageClick}
                                                containerClassName={'pagination'}
                                                subContainerClassName={'pages pagination'}
                                                activeClassName={'active'}
                                            />
                                        </Row>
                                    </Container>
                                </div>
                            }
                        </Tab>
                    </Tabs>
                </Container>
            </div>
        );
    }
}
