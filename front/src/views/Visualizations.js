import React, { Component } from 'react';
import { Tab, Tabs, Container } from 'react-bootstrap';
import "./Visualization.css";
import BarGraph from './BarGraph';
import BubbleChart from './BubbleChart';
import PieChart from './PieChart';
import DeveloperVisualization1 from './DeveloperVisualization1';
import DevBubble from './DevBubble'
import DevPieChart from './DevPieChart'

export default class Visualizations extends Component {
    render() {
        document.body.style.backgroundImage = "none";
        return (
            <div>
                <h1 style={{ textAlign: "center", margin: "25px" }}>Visualizations</h1>
                <Container>
                    <Tabs defaultActiveKey="Vis1" id="Tabs">
                        <Tab eventKey="Vis1" title="Visualization 1">
                            <BarGraph />
                        </Tab>
                        <Tab eventKey="Vis2" title="Visualization 2">
                            <BubbleChart />
                        </Tab>
                        <Tab eventKey="Vis3" title="Visualization 3">
                            <PieChart />
                        </Tab>
                        <Tab eventKey="DevVis1" title="Developer Vis 1">
                            <DeveloperVisualization1 />
                        </Tab>
                        <Tab eventKey="DevVis2" title="Developer Vis 2">
                            <DevBubble />
                        </Tab>
                        <Tab eventKey="DevVis3" title="Developer Vis 3">
                            <DevPieChart />
                        </Tab>
                    </Tabs>
                </Container>
            </div>
        );
    }
}